#include "Header.h"

void import_course() {
	char path[100];
	List <Course> courses;
	cout << "Import courses from a csv file: \n";
	cout << "Path to .csv file: ";
	cin.getline(path, 101);
	if(!load_course(courses, path))
		cout << "Error loading file!\n";
	else {
		save_course("D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv", courses);
		remove_listCourses(courses);
		cout << "Successful import!\n";
	}

}
bool load_course(List <Course> &a, char path[]){
	ifstream fin(path);
	if (!fin.is_open())
		return false;
	Course course;
	Node <Course> *cur = NULL;
	Node <Course> *p = NULL;
	a.head = NULL;
	char header[200];
	fin.getline(header, 201);
	while (!fin.eof()) {

		fin.getline(course.code, 11, ',');
		fin.getline(course.year, 51, ',');
		fin >> course.semester;
		fin.ignore(1000, ',');
		fin.getline(course.name, 101, ',');
		fin.getline(course.lecturer_user, 21, ',');

		fin >> course.start.day >> course.start.month >> course.start.year;

		fin.ignore(1000, ',');

		fin >> course.end.day >> course.end.month >> course.end.year;

		fin.ignore(1000, ',');
		fin >> course.from.hour >> course.from.minute;
		fin.ignore(1000, ',');


		fin >> course.to.hour >> course.to.minute;
		fin.ignore(1000, ',');

		fin.get(course.date_of_week, 101, '\n');

		fin.ignore(1000, '\n');

		p = new Node <Course>;
		p->data = course;
		p->next = NULL;
		if (a.head == NULL)
		{
			a.head = p;
			cur = a.head;
			cur->prev = NULL;
		}
		else
		{
			cur->next = p;
			p->prev = cur;
			cur = p;
		}
	}
	fin.close();
	return true;
}

void save_course(char path[], List <Course> a) {
	ofstream fout(path);
	Node <Course> *cur = a.head;
	fout << "Course code,year,semester,course name,lecturer username,start at,end at,from,to,date of week" << endl;
	while (cur != NULL) {
		fout << cur->data.code << ",";

		fout << cur->data.year << ",";

		fout << cur->data.semester << ",";

		fout << cur->data.name << ",";

		fout << cur->data.lecturer_user << ",";

		fout << cur->data.start.day << " " << cur->data.start.month << " " << cur->data.start.year << ",";
		fout << cur->data.end.day << " " << cur->data.end.month << " " << cur->data.end.year << ",";
		
		fout << cur->data.from.hour << " " << cur->data.from.minute << ",";
		fout << cur->data.to.hour << " " << cur->data.to.minute << ",";

		fout << cur->data.date_of_week;
			
		if(cur->next)
			fout << endl;
		cur = cur->next;
	}
	fout.close();
}

Node <Course> *same_course(char str[], List <Course> a) {
	Node <Course> *cur = a.head;
	while (cur != NULL)
	{
		if (_stricmp(str,cur->data.code)==0)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

void input_with_code(Course &course) {
	cin.ignore(1000, '\n');

	cout << "Please input the year of this course(Ex:2016 2017): ";
	cin.get(course.year, 21);

	cout << "What is the semester the course will be learned: ";
	cin >> course.semester;

	cin.ignore(1000, '\n');
	cout << "What is the name of this course: ";
	cin.get(course.name, 101);

	cout << "What is the lecturer user name(Ex: htthanh): ";
	cin >> course.lecturer_user;

	cout << "The course will start on: " << endl;
	cout << "	Day: "; cin >> course.start.day;
	cout << "	Month: "; cin >> course.start.month;
	cout << "	Year: "; cin >> course.start.year;

	cout << "It will end on: " << endl;
	cout << "	Day: "; cin >> course.end.day;
	cout << "	Month: "; cin >> course.end.month;
	cout << "	Year: "; cin >> course.end.year;
	cout << "The class will begin at: " << endl;
	cout << "	Hour: "; cin >> course.from.hour;
	cout << "	Minute: "; cin >> course.from.minute;

	cout << "And it will finish at: " << endl;
	cout << "	Hour: "; cin >> course.to.hour;
	cout << "	Minute: "; cin >> course.to.minute;

	cout << "On: (Ex: Monday): ";
	cin >> course.date_of_week;
}
void add_course(List <Course> &a) {
	Course course;
	cout << "WELCOME TO THE PROGRAM ADD A NEW COURSE!!!" << endl;
	cout << "Please input the course code: ";
	cin >> course.code;
	while (same_course(course.code, a) != NULL)
	{
		cout << "This course existed. Please input another code of this course: ";
		cin >> course.code;
	}

	input_with_code(course);

	Node <Course> *add = NULL;
	Node <Course> *cur = a.head;
	while (cur->next != NULL) 
		cur = cur->next;
	if (cur->next == NULL)
	{
		Node <Course> *add = new Node<Course>;
		add->data = course;
		cur->next = add;
		add->next = NULL;
	}
}
void menu_course() {
	cout << "Which one you want to edit: " << endl;
	cout << "0. Year" << endl;
	cout << "1. Semester" << endl;
	cout << "2. Course name" << endl;
	cout << "3. Lecturer username" << endl;
	cout << "4. When staring this course" << endl;
	cout << "5. When ending this course" << endl;
	cout << "6. What time this course will start" << endl;
	cout << "7. What time this course will end" << endl;
	cout << "8. This course will be learned on (Ex: Monday)"<<endl;
}
void edit_course(List <Course> &a) {
	Course course;
	cout << "WELCOM TO THE PROGRAM EDIT INFORMATION OF COURSE EXISTED!!!" << endl << endl;
	cout << "Please input the course's code you want to edit: ";
	cin.getline(course.code, 12);
	Node <Course> *temp = same_course(course.code, a);
	while (same_course(course.code, a) == NULL)
	{
		cout << "This course is not existed. Please input another: ";
		cin.getline(course.code, 12);
	}
edit_Start:
	system("cls");
	bool Flag = true;
	menu_course();
	cout << "Which one you want to choose (Press other numbers to exit): ";
	char s[10];
	int c;
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return;
	}
	c = atoi(s);
	switch (c) {
	case 0:
		cout << "Please input the new year of " << course.code << ": ";
		cin.getline(course.year, 21);
		strcpy(temp->data.year, course.year);
		break;

	case 1:
		cout << "Please input the new semester of " << course.code << ": ";
		cin >> course.semester;
		temp->data.semester = course.semester;
		cout << endl;
		break;

	case 2:
		cout << "Please input the new name of " << course.code << ": ";
		cin.getline(course.name, 101);
		strcpy(temp->data.name, course.name);
		break;

	case 3:
		cout << "Please input the new lecturer username of " << course.code << ": ";
		cin.getline(course.lecturer_user, 22);
		strcpy(temp->data.lecturer_user, course.lecturer_user);
		break;

	case 4:
		cout << "Please input the new date starting of " << course.code << ": " << endl;
		cout << "	Day: "; cin >> course.start.day; cin.ignore();
		cout << "	Month: "; cin >> course.start.month; cin.ignore();
		cout << "	Year: "; cin >> course.start.year; cin.ignore();
		temp->data.start = course.start;
		break;

	case 5:
		cout << "Please input the new date ending of " << course.code << ": " << endl;

		cout << "	Day: "; cin >> course.end.day; cin.ignore();
		cout << "	Month: "; cin >> course.end.month; cin.ignore();
		cout << "	Year: "; cin >> course.end.year; cin.ignore();

		temp->data.end = course.end;

		break;

	case 6:
		cout << "Please input the time this course will start: ";
		cout << "The class will begin at: " << endl;
		cout << "	Hour: "; cin >> course.from.hour; cin.ignore();
		cout << "	Minute: "; cin >> course.from.minute; cin.ignore();

		temp->data.from = course.from;
		break;
	case 7:

		cout << "Please input the time this course will end: ";
		cout << "The class will end at: " << endl;
		cout << "	Hour: "; cin >> course.to.hour; cin.ignore();
		cout << "	Minute: "; cin >> course.to.minute; cin.ignore();
		temp->data.to = course.to;
		break;
	case 8:

		cout << "This course will be learned on: ";
		cin.getline(course.date_of_week, 102);
		strcpy(temp->data.date_of_week, course.date_of_week);
		break;
	default:
		Flag = false;
		break;
	}
	if (Flag == false)	return;
	else {
		cout << "Successful editing!\n";
		system("pause");
		goto edit_Start;
	}
}

void remove_course(List <Course> &a) {
	Course course;
	cout << "WELCOME TO THE PROGRAM REMOVE A COURSE!!!" << endl;
	cout << "Please input the code of the course you want to remove: ";
	cin >> course.code;
	while (same_course(course.code, a) == NULL)
	{
		cout << "This course is not existed. Please input another or input 0 to exit: ";
		cin >> course.code;
		if (strcmp(course.code, "0") == 0)
			return;
	}
	Node <Course> *course_seach = same_course(course.code, a);
	if (a.head == course_seach)
	{
		Node <Course> *temp = a.head;
		a.head = a.head->next;
		a.head->prev = NULL;
		delete temp;
	}
	else {
		Node <Course> *cur = a.head;

		while (cur->next != course_seach) {
			cur = cur->next;
		}

		Node <Course> *temp = cur->next;
		if (cur->next->next == NULL) {
			cur->next = NULL;
		}
		else {
			cur->next = cur->next->next;
			temp->next->prev = cur->prev;
		}
		delete temp;
	}
	cout << "THIS COURSE HAS BEEN REMOVED SUCCESFULLY!!!" << endl;
}

void view_course(List <Course> a) {
	//cout << "WELCOM TO THE PROGRAM WIEW LIST OF COURSE!!!" << endl;
	cout << "List of courses: " << endl;
	Node <Course> *cur = a.head;
	int i = 1;
	while (cur != NULL) {
		cout << i << ". " << cur->data.code << endl;

		cout << "	Year: " << cur->data.year << endl;

		cout << "	Semester: " << cur->data.semester << endl;

		cout << "	Course's Name: " << cur->data.name << endl;

		cout << "	Lecturer Username: " << cur->data.lecturer_user << endl;

		cout << "	Start At: " << cur->data.start.day << " " << cur->data.start.month << " " << cur->data.start.year << endl;
		cout << "	End At: " << cur->data.end.day << " " << cur->data.end.month << " " << cur->data.end.year << endl;

		cout << "	From: " << cur->data.from.hour << " " << cur->data.from.minute << endl;
		cout << "	To: " << cur->data.to.hour << " " << cur->data.to.minute << endl;

		cout << "	Will be learned on: " << cur->data.date_of_week << endl;

		cur = cur->next;
		i++;
	}
}

void remove_listCourses(List <Course> &a) {
	Node <Course> *temp = a.head;
	while (a.head) {
		temp = a.head;
		a.head = a.head->next;
		if(a.head)
			a.head->prev = NULL;
		delete temp;
		temp = NULL;
	}
}