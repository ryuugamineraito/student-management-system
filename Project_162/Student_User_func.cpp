#include "Header.h"
void import_class() {
	char path[100];
	Classroom a;
	cout << "Import students of a class from a csv file: \n";
	cout << "Path to .csv file: ";
	cin.getline(path, 101);
	if (!load_class(a, path)) {
		cout << "Error loading file!\n";
	}
	else {
		List <Classroom> classes = import_listClass();
		if (!check_listClass(classes, a)) {
			cout << "Existed Class!\n";
		}
		else {
			char pathOut[100] = "D:\\Learn\\class\\CS\\PJ\\Students\\";
			strcat(pathOut, a.className);
			strcat(pathOut, ".csv");
			save_Class(pathOut, a);
			cout << "Successful import!\n";
		}
		remove_allStudents_class(a);
		remove_listClass(classes);
	}
}
bool load_class(Classroom &a, char path[]) {		//load students of a class from a csv file
	a.student.head = NULL;
	ifstream fin(path);
	if (!fin)	return false;
	else {
		char header1[100], header2[100], Cls[100];
		fin.get(header1, 100, ',');
		fin.ignore();
		fin.getline(Cls, 100);
		fin.ignore();
		fin.get(header2, 100);
		fin.ignore();
		strcpy(a.className, Cls);

		char no[30], id[100], name[100], email[100], phone[100], pass[100];
		int type;
		Node <User> *cur = a.student.head;
		while (!fin.eof()) {
			fin.get(no, 30, ',');
			fin.ignore(1000, ',');
			fin.get(id, 100, ',');
			fin.ignore(1000, ',');
			fin.get(name, 100, ',');
			fin.ignore(1000, ',');
			fin.get(email, 100, ',');
			fin.ignore(1000, ',');
			fin.get(phone, 100, ',');
			fin.ignore(1000, ',');
			fin.get(pass, 100, ',');
			fin.ignore(1000, ',');
			fin >> type;
			// Create a new node and pass data to it.
			Node <User> *add = new Node <User>;
			strcpy(add->data.userName, id);
			strcpy(add->data.fullname, name);
			strcpy(add->data.email, email);
			strcpy(add->data.pass, pass);
			add->data.type = type;
			strcpy(add->data.phone, phone);
			strcpy(add->data.Class, Cls);
			add->next = NULL;
			add->prev = NULL;
			// push the node to the list.
			if (!a.student.head) {
				a.student.head = add;
				cur = a.student.head;
			}
			else {
				cur->next = add;
				add->prev = cur;
				cur = add;
			}
		}
	}
	fin.close();
	return true;
}

void import_aStudent_Screen(User &student) {	//input a student from screen
	student.type = 0;
	cout << "ID: ";
	cin.getline(student.userName, 100);
	cout << "Student name: ";
	cin.getline(student.fullname, 100);
	cout << "Class: ";
	cin.getline(student.Class, 100);
	cout << "Email: ";
	cin.getline(student.email, 100);
	cout << "Pass: ";
	cin.getline(student.pass, 100);
	cout << "Mobile phone: ";
	cin >> student.phone;
}

void import_aStudent_noClass_Screen(User &student) {	//input a student from screen without class
	student.type = 0;
	cout << "ID: ";
	cin.getline(student.userName, 100);
	cout << "Student name: ";
	cin.getline(student.fullname, 100);
	cout << "Email: ";
	cin.getline(student.email, 100);
	cout << "Pass: ";
	cin.getline(student.pass, 100);
	cout << "Mobile phone: ";
	cin >> student.phone;
}
bool add_aStudent_toClass(Classroom &a) {	//add a student to a class
	//if (!a.student.head) return false;
	Node <User> *q = a.student.head;
	while (q && q->next) {
		q = q->next;
	}
	Node <User> *temp = new Node <User>;
	import_aStudent_noClass_Screen(temp->data);
	if (!isExisted_ID(a, temp->data.userName)) {
		strcpy(temp->data.Class, a.className);
		temp->next = NULL;
		temp->prev = NULL;
		if (!a.student.head) {
			a.student.head = temp;
		}
		else {
			q->next = temp;
			temp->prev = q;
		}
		return true;
	}
	else {
		cout << "Existed student!\n";
		return false;
	}
}

Node <User> *find_aStudent(Classroom a, char id[]) {	//find a student in a class.
	if (a.student.head == NULL)
		return NULL;
	Node <User> *p = a.student.head;
	while (p) {
		if (strcmp(p->data.userName, id) == 0)
			return p;
		p = p->next;
	}
	return NULL;
}

//if exist, return true, not exist return false.
bool isExisted_ID(Classroom a, char *Id) {
	Node <User> *std = a.student.head;
	string tmp2 = Id;
	while (std) {
		string tmp = std->data.userName;
		if (Id == tmp)
			return true;
		std = std->next;
	}
	return false;
}
bool edit_aStudent(Classroom &a) {		//edit a student in a class.
	if (!a.student.head)
		return false;
	char id[100];
	cout << "ID of the student need to be edited: ";
	cin.getline(id, 100);
	Node <User> *temp = find_aStudent(a, id);
	//check that id is in classroom or not, if not, yeu cau ng dung nhap lai
	while (temp == NULL) {
		cout << "Illegal!!\n";
		cout << "Retry or press 0 to exit: ";
		cin.getline(id, 100);
		if (strcmp(id, "0") == 0)
			break;
		temp = find_aStudent(a, id);
	}
	if (!temp)	return false;
	loop:
	system("cls");
	cout << "0. Name\n1. Email\n2. Phone\n3. Exit editing\n";
	cout << "Which one you want to edit (Press other keys to exit): ";
	char s[10];
	int c;
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return false;
	}
	c = atoi(s);
	char tmp[100];
	switch (c) {
	case 0:
		cout << "Current Name: " << temp->data.fullname << endl;
		cout << "New Name: ";
		cin.getline(tmp, 100);
		strcpy(temp->data.fullname, tmp);
		goto loop;
		break;
	case 1:
		cout << "Current Email: " << temp->data.email << endl;
		cout << "New Email: ";
		cin.getline(tmp, 100);
		strcpy(temp->data.email, tmp);
		goto loop;
		break;
	case 2:
		cout << "Current Phone number: " << temp->data.phone << endl;
		cout << "New Phone number: ";
		cin.getline(tmp, 100);
		strcpy(temp->data.phone, tmp);
		goto loop;
		break;
	case 3:
		break;
	default:
		return false;
	}
	return true;
}

bool remove_aStudent(Classroom &a) {
	if (!a.student.head)
		return false;
	char id[100];
	cout << "ID of the student need to be removed: ";
	cin.getline(id, 100);
	Node <User> *temp = find_aStudent(a, id);
	//check that id is in classroom or not, if not, yeu cau ng dung nhap lai.
	while (temp == NULL) {
		cout << "Illegal!!\n";
		cout << "Retry or press 0 to exit: ";
		cin.getline(id, 100);
		if (strcmp(id, "0") == 0)
			break;
		temp = find_aStudent(a, id);
	}
	if (!temp)	return false;
	// find that student and now we delete.
	if (temp == a.student.head) {
		a.student.head = a.student.head->next;
		if(a.student.head)
			a.student.head->prev = NULL;
		delete temp;
		temp = NULL;
		return true;
	}
	if (temp->prev) {
		temp->prev->next = temp->next;
	}
	if (temp->next) {
		temp->next->prev = temp->prev;
	}
	temp->next = NULL;
	temp->prev = NULL;
	delete temp;
	temp = NULL;
	return true;
}

void remove_allStudents_class(Classroom &a) {
	if (!a.student.head)	return;
	else {
		while (a.student.head) {
			Node <User> *temp = a.student.head;
			a.student.head = a.student.head->next;
			temp = NULL;
			delete temp;
		}
	}
}

Classroom add_newEClass(char *name) {
	Classroom a;
	strcpy(a.className, name);
	a.student.head = NULL;
	return a;
}

void view_listClass(List <Classroom> a) {
	Node <Classroom> *temp = a.head;
	if (!a.head) {
		cout << "Empty list of classes!\n";
		return;
	}
	cout << "List of Classes: \n";
	int t = 1;
	while (temp) {
		cout << t++ << ". " << temp->data.className << endl;
		temp = temp->next;
	}
}

void view_listStudents(Classroom a) {
	Node <User> *temp = a.student.head;
	if (!a.student.head) {
		cout << "Empty Class!" << endl;
	}
	else {
		cout << "Students of this class: \n";
		int t = 1;
		while (temp) {
			cout << t++ << ". ID: " << temp->data.userName << "\tName: " << temp->data.fullname << endl;
			temp = temp->next;
		}
	}
}

void change_Students(Classroom &a, Classroom &b) {
	char id[100];
	cout << "ID of the student you want to change: ";
	cin.getline(id, 100);
	Node <User> *temp = find_aStudent(a, id);
	while (temp == NULL) {
		cout << "Illegal!!\n";
		cout << "Retry or press 0 to exit: ";
		cin.getline(id, 100);
		if (strcmp(id, "0") == 0)
			break;
		temp = find_aStudent(a, id);
	}
	if (!temp)	return;
	if (temp == a.student.head) {
		a.student.head = temp->next;
	}
	if (temp->next != NULL)	temp->next->prev = temp->prev;
	if (temp->prev != NULL)	temp->prev->next = temp->next;
	if (!b.student.head) {
		temp->next = temp->prev = NULL;
		b.student.head = temp;
	}
	else {
		Node <User> *tail = b.student.head;
		while (tail && tail->next) {
			tail = tail->next;
		}
		tail->next = temp;
		temp->next = NULL;
		temp->prev = tail;
	}
	strcpy(temp->data.Class, b.className);
	cout << "Compelete exchange!\n";
}

void save_Class(char *path, Classroom a) {
	add_tolistClass(a);
	ofstream fout(path);
	fout << "Class," << a.className << endl;
	if (!a.student.head)
		fout << "No,Student ID,Student name,Email,phone,Password,Type";
	else
		fout << "No,Student ID,Student name,Email,phone,Password,Type\n";
	int i = 1;
	Node <User> *temp = a.student.head;
	while (temp) {
		if (!temp->next) {
			fout << i << "," << temp->data.userName << "," << temp->data.fullname << "," << temp->data.email << "," << temp->data.phone << "," << temp->data.pass << "," << temp->data.type;
		}
		else {
		fout << i << "," << temp->data.userName << "," << temp->data.fullname << "," << temp->data.email << "," << temp->data.phone << "," << temp->data.pass << "," << temp->data.type << endl;
		}
		temp = temp->next;
		i++;
	}
	fout.close();
}

void saveUser(char *path, char user[][20], char pass[][20], int n) {
	ofstream fout(path);
	for (int i = 0; i < n; i++) {
		if (i == n - 1)
			fout << user[i] << "," << pass[i];
		else
			fout << user[i] << "," << pass[i] << endl;
	}
	fout.close();
}

bool check_listClass(List <Classroom> classes, Classroom a) {
	if (!classes.head)	return true;
	Node <Classroom> *cur = classes.head;
	while (cur) {
		if (strcmp(cur->data.className, a.className) == 0) {
			return false;
		}
		cur = cur->next;
	}
	return true;
}

List <Classroom> import_listClass(){
	ifstream fin("D:\\Learn\\class\\CS\\PJ\\Students\\ListOfClasses.csv");
	List <Classroom> classes;
	classes.head = NULL;
	Node <Classroom> *cur = classes.head;
	if (!fin.good())	return classes;
	char clsName[50];
	//cout << "a";
	while (!fin.eof()) {
		fin.getline(clsName, 50);
		if (!classes.head && strcmp(clsName, "") == 0) {
			fin.close();
			return classes;
		}
		Node <Classroom> *temp = new Node <Classroom>;
		strcpy(temp->data.className, clsName);
		temp->next = NULL;
		temp->prev = NULL;
		if (classes.head == NULL) {
			cur = classes.head = temp;
		}
		else {
			cur->next = temp;
			temp->prev = cur;
			cur = temp;
		}
	}
	fin.close();
	return classes;
}

void remove_listClass(List <Classroom> &a) {
	while (a.head) {
		Node <Classroom> *tmp = a.head;
		a.head = a.head->next;
		delete tmp;
		tmp = NULL;
	}
}

bool add_tolistClass(Classroom a) {
	bool Flag;
	List <Classroom> classes = import_listClass();
	if (check_listClass(classes, a)) {
		ofstream fout;
		if (!classes.head) {
			fout.open("D:\\Learn\\class\\CS\\PJ\\Students\\ListOfClasses.csv");
		}
		else
			fout.open("D:\\Learn\\class\\CS\\PJ\\Students\\ListOfClasses.csv", ios::app);

		if(classes.head)
			fout << endl;
		fout << a.className;
		fout.close();
		Flag = true;
	}
	else {
		Flag = false;
	}
	remove_listClass(classes);
	return Flag;
}

int Student(Node <User> *student) {
	if (!student)	return -1;
	std_Start:
	system("cls");
	cout << "Welcome " << student->data.fullname << endl;
	cout << "0. Show menu\n1. View info\n2. Change password\n3. Logout\n";
	cout << "Which one you want to choose (Press other numbers to exit): ";
	char tmp[50];
	string a;
	char s[10];
	int c;
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return -1;
	}
	c = atoi(s);
	switch (c) {
	case 0:
		menu_Student(student);
		goto std_Start;
		break;
	case 1:
		cout << "Class: " << student->data.Class << endl;
		cout << "Full name: " << student->data.fullname << endl;
		cout << "ID(Username): " << student->data.userName << endl;
		cout << "Pass: " << student->data.pass << endl;
		cout << "Email: " << student->data.email << endl;
		cout << "Phone: " << student->data.phone << endl;
		system("pause");
		goto std_Start;
		break;
	case 2:
		cout << "Change password.\n";
		cout << "New password: ";
		a = passwordInput(20);
		CpyStrnChar(a, tmp);
		while(a == "1") {
			system("cls");
			cout << "Password must be different from 1\n";
			cout << "New Password: ";
			a = passwordInput(20);
		}
		CpyStrnChar(a, tmp);
		strcpy(student->data.pass, tmp);
		cout << "Successful!\n";
		system("pause");
		goto std_Start;
		break;
	case 3:
		return 0;
	default:
		return -1;
	}
}

void menu_Student(Node<User> *student) {
Start_menu:
	system("cls");
	cout << "0. Check-in\n";
	cout << "1. View check-in result\n";
	cout << "2. View your scores of a course\n";
	cout << "3. View schedules\n";
	cout << "Which one you want to choose (Press other numbers to exit): ";
	char s[10];
	int c;
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return;
	}
	c = atoi(s);
	switch (c) {
	case 0:
		system("cls");
		check_in(student);
		system("pause");
		goto Start_menu;
		break;
	case 1:
		system("cls");
		viewcheckInresult(student);
		system("pause");
		goto Start_menu;
		break;
	case 2:
		system("cls");
		List <Course> a;
		load_course(a, "D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv");
		view_score_student(student->data.userName, a);
		remove_listCourses(a);
		system("pause");
		goto Start_menu;
		break;
	case 3:
		system("cls");
		viewSchedule(student);
		system("pause");
		goto Start_menu;
		break;
	default:
		return;
	}
}

void load_arrayClass(Classroom *a, int &nCl) {
	char pathChar[50]; int i = 0;
	List <Classroom> temp = import_listClass();
	if (!temp.head) {
		nCl = 0;
		return;
	}
	Node <Classroom> *cur = temp.head;
	while (cur) {
		string cls = cur->data.className;
		string path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		load_class(a[i], pathChar);
		i++;
		cur = cur->next;
	}
	remove_listClass(temp);
	nCl = i;
}

void remove_arrayClass(Classroom *a, int nCl) {
	for (int i = 0; i < nCl; i++) {
		remove_allStudents_class(a[i]);
	}
}