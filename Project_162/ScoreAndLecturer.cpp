#include "Header.h"


int lecturer(int index, char userL[][20], char passL[][20], int nL) {
	L_start:
	system("cls");
	cout << "Welcome Lecturer!\n";
	cout << "0. Show menu\n1. View info\n2. Change password\n3. Logout\n";
	cout << "Which one you want to choose (Press other numbers to exit): ";
	int flagL;
	char s[10];
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return -1;
	}
	flagL = atoi(s);
	switch (flagL) {
	case 0:
		menu_Lecturer(userL[index]);
		goto L_start;
		break;
	case 1:
		cout << "User: " << userL[index] << endl;
		cout << "Pass: " << passL[index] << endl;
		system("pause");
		goto L_start;
		break;
	case 2:
		system("cls");
		cout << "Change password!\n";
		change_pass(passL, index);
		saveUser("D:\\Learn\\class\\CS\\PJ\\Project_162\\Lecturer.csv", userL, passL, nL);
		goto L_start;
		break;
	case 3:
		return 0;
	default :
		return -1;
	}
}

void menu_Lecturer(char *L_user) {
	system("cls");
	List <Course> courses;
	load_course(courses, "D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv");
	if (!check_lecturer(courses, L_user)) {
		cout << "You don't have any course, pls contact Academic Staff!\n";
		system("pause");
	}
	else {
		view_coursesOfLecturer(courses, L_user);
		cout << "Choose a course(Ex: CS162, MTH251,...): ";
		char tmpCode[50];
		cin.getline(tmpCode, 50);
	chooseCourse:
		converttouppercase(tmpCode);
		if (!check_courseOfLecturer(courses, L_user, tmpCode)) {
			cout << "You don't have this course!\nRetry or press 0 to exit: ";
			cin.getline(tmpCode, 50);
			if (strcmp(tmpCode, "0") == 0) {
				goto END;
			}
			goto chooseCourse;
		}
		else {
			Course_Score a;
			char pathScore[50] = "D:\\Learn\\class\\CS\\PJ\\Score\\";
			strcat(pathScore, tmpCode);
			strcat(pathScore, ".csv");
		Choose:
				load_scoreboard(pathScore, a);
				system("cls");
				cout << "0. Import scoreboard of a course\n";
				cout << "1. Edit grade of a student\n";
				cout << "2. View a scoreboard\n";
				cout << "Which one you want to choose (Press other numbers to exit): ";
				char s[10];
				int c;
				cin.getline(s, 10);
				for (int i = 0; i < strlen(s); i++) {
					if (s[i] < '0' || s[i] > '9')
						return;
				}
				c = atoi(s);
				switch (c) {
				case 0:
					system("cls");
					import_scoreboard(tmpCode);
					remove_course_score(a);
					system("pause");
					goto Choose;
					break;
				case 1:
					if (edit_grade(a)) {
						cout << "Successful editing!\n";
						export_scoreboard(pathScore, a);
					}
					else
						cout << "Fail editing!\n";
					remove_course_score(a);         
					system("pause");
					goto Choose;
					break;
				case 2:
					view_scoreboard_course(a);
					remove_course_score(a);
					system("pause");
					goto Choose;
					break;
				default:
					remove_course_score(a);
					break;
				}
		}
	}
END:
	remove_listCourses(courses);
}

bool check_lecturer(List <Course> courses, char *L_user) {
	Node <Course> *tmp = courses.head;
	while (tmp) {
		if (strcmp(tmp->data.lecturer_user, L_user) == 0) {
			return true;
		}
		tmp = tmp->next;
	}
	return false;
}

void view_coursesOfLecturer(List <Course> courses, char *L_user) {
	Node <Course> *tmp = courses.head;
	int i = 0;
	cout << "Your courses: \n";
	while (tmp) {
		if (strcmp(tmp->data.lecturer_user, L_user) == 0) {
			cout << ++i << ". " << tmp->data.code << endl;
		}
		tmp = tmp->next;
	}
	if (i == 0)
		cout << "Empty!\n";
}

bool check_courseOfLecturer(List <Course> courses, char *L_user, char *codeCourse) {
	Node <Course> *tmp = courses.head;
	while (tmp) {
		if (strcmp(tmp->data.lecturer_user, L_user) == 0 && strcmp(tmp->data.code, codeCourse) == 0) {
			return true;
		}
		tmp = tmp->next;
	}
	return false;
}

void import_scoreboard(char tmpcode[]) {
	char path[100];
	Course_Score b;
	cout << "Import scoreboard from a csv file: \n";
	cout << "Path to .csv file: ";
	cin.getline(path, 101);
	if (!load_scoreboard(path, b)) {
		cout << "Error loading file!\n";
	}
	else {
		if (_stricmp(tmpcode, b.code) == 0) {
			char pathOut[100] = "D:\\Learn\\class\\CS\\PJ\\Students\\";
			strcat(pathOut, b.code);
			strcat(pathOut, ".csv");
			export_scoreboard(pathOut, b);
		}
		else {
			cout << "The file you input is not scoreboard of this course!\n";
		}
		remove_course_score(b);
	}
}

bool load_scoreboard(char path[], Course_Score &b) {
	ifstream fin;
	fin.open(path);
	if (!fin.is_open())
		return false;
	Score score;
	Node <Score> *cur = NULL;
	Node <Score> *p = NULL;
	b.score.head = NULL;
	char name_code[11], name_year[11], name_semester[11], list[1000];
	//fin >> name_code >> b.code;
	fin >> name_code >> b.code;
	fin.get(name_year, 11, ' ');
	fin.ignore(1000, ' ');
	fin.get(b.year, 11, '\n');
	fin >> name_semester >> b.semester;
	fin.ignore();
	fin.getline(list, 1000, '\n');
	//fin.ignore(1000, '\n');

	while (!fin.eof()){
		fin.get(score.ID,11,',');
		fin.ignore(1000, ',');

		fin >> score.midterm;
		fin.ignore(1000, ',');

		fin >> score.lab; 
		fin.ignore(1000, ',');

		fin >> score.final;
		fin.ignore(1000, ',');

		fin >> score.bonus;
		fin.ignore(1000, '\n');
		p = new Node <Score>;
		p->data = score;
		p->next = NULL;
		if (b.score.head == NULL)
		{
			b.score.head = p;
			cur = b.score.head;
			cur->prev = NULL;
		}
		else
		{
			cur->next = p;
			p->prev = cur;
			cur = p;
		}
	}
	fin.close();
	return true;
}

void export_scoreboard(char path[], Course_Score b) {
	ofstream fout;
	fout.open(path);
	if (!b.score.head)
		return;
	Node <Score> *cur = b.score.head;
	fout << "Code: " << b.code << endl << "Year: " << b.year << endl << "Semester: " << b.semester << endl;
	fout << "ID,Midterm,Lab,Final,Bonus" << endl;
	while (cur != NULL) {
		fout << cur->data.ID << ",";
		fout << cur->data.midterm << ",";
		fout << cur->data.lab << ",";
		fout << cur->data.final << ",";
		if (cur->next == NULL)
			fout << cur->data.bonus;
		else
			fout << cur->data.bonus << endl;

		cur = cur->next;
	}
}

Node <Score> *same_ID(char str[], Course_Score b) {
	if (b.score.head == NULL)
		return NULL;
	Node <Score> *cur = b.score.head;
	while (cur != NULL)
	{
		if (strcmp(str, cur->data.ID) == 0)
			return cur;
		cur = cur->next;
	}
	return NULL;
}
void menu_score() {
	cout << "0. Midterm" << endl;
	cout << "1. Lab" << endl;
	cout << "2. Final" << endl;
	cout << "3. Bonus" << endl;
	cout << "Which one you want to choose (Press other numbers to exit): ";
}
//it just can be edit one time.
bool edit_grade(Course_Score &b) {
	Score scr;
	if (b.score.head == NULL)
		return false;
	char id[11];
	int choose;
	system("cls");
	cout << "WELCOME TO THE PROGRAM EDIT GRADE OF STUDENT!!!" << endl;
	cout << "Please input the ID of student you want to edit grade: ";
	cin>>id;
	while (same_ID(id, b) == NULL) {
		cout << "This student is not existed in your course. Please input again!(Or input 0 to exit): ";
		cin >> id;
		if (strcmp(id, "0") == 0)
			return false;
	}
	Node <Score> *temp = same_ID(id, b);
	menu_score();
	cin >> choose;
	switch (choose){
	case 0:
		cout << "Current grade of midterm: " << temp->data.midterm << endl;
		cout << "Please input the new grade of midterm: ";
		cin >> scr.midterm;
		temp->data.midterm = scr.midterm;
		break;
	case 1:
		cout << "Current grade of lab: " << temp->data.lab << endl;
		cout << "Please input the new grade of lab: ";
		cin >> scr.lab;
		temp->data.lab = scr.lab;
		break;
	case 2:
		cout << "Current grade of final: " << temp->data.final << endl;
		cout << "Please input the new grade of final: ";
		cin >> scr.final;
		temp->data.final = scr.final;
		break;
	case 3:
		cout << "Current grade of bonus: " << temp->data.bonus << endl;
		cout << "Please input the new bonus of final: ";
		cin >> scr.bonus;
		temp->data.bonus = scr.bonus;
		break;
	default:
		cin.ignore();
		return false;
	};
	cin.ignore();
	return true;
}
void view_scoreboard_course(Course_Score b) {
	system("cls");
	if (b.score.head == NULL) {
		cout << "Empty Course!";
		return;
	}
	cout << "Course: " << b.code << "	Semester: " << b.semester << "	Year: " << b.year << endl;
	Node <Score> *cur = b.score.head;
	int i = 0;
	while (cur != NULL) {
		cout << ++i << ". ID: " << cur->data.ID << endl;
		cout << "	Grade of midterm: " << cur->data.midterm << endl;
		cout << "	Grade of lab: " << cur->data.lab << endl;
		cout << "	Grade of final: " << cur->data.final << endl;
		cout << "	Grade of bonus: " << cur->data.bonus << endl;
		cur = cur->next;
	}
}
void remove_course_score(Course_Score &b) {
	if (!b.score.head)
		return;
	Node <Score> *temp = b.score.head;
	while (b.score.head != NULL) {
		b.score.head = b.score.head->next;
		delete temp;
		temp = b.score.head;
	}
}

void list_score_student(char id[], Course_Score b) {

	Node <Score> *scr_stu = same_ID(id, b);
	cout << "Code: " << b.code << endl << "Year: " << b.year << endl << "Semester: " << b.semester << endl;
	cout << "ID: " << scr_stu->data.ID << endl;
	cout << "	Midterm: " << scr_stu->data.midterm << endl;
	cout << "	Lab: " << scr_stu->data.lab << endl;
	cout << "	Final: " << scr_stu->data.final << endl;
	cout << "	Bonus: " << scr_stu->data.bonus << endl;
}
void view_score_student(char id[],  List <Course> a) {
	Course_Score b;
	string code, path;
	char pathChar[100];
	cout << "WELCOME TO THE PROGRAM VIEW SCORE OF STUDENT!!!\n";
	cout << "Code of course you want to view score(Input 0 to exit): ";
	getline(cin, code);
	CpyStrnChar(code, b.code);
	if (strcmp(b.code, "0") == 0)
		return;
	converttouppercase(code);
	path = "D:\\Learn\\class\\CS\\PJ\\Score\\" + code + ".csv";
	CpyStrnChar(path, pathChar);
	if (same_course(b.code, a) != 0) {
		if (load_scoreboard(pathChar, b)) {
			if (same_ID(id, b) == NULL) {
				cout << "You haven't have score for this course!\n";
			}
			else list_score_student(id, b);
			remove_course_score(b);
		}
		else {
			cout << "No scoreboard of this course now!" << endl;
		}
	}
	else {
		cout << "This course is not existed\n";
	}
}

