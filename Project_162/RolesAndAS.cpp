﻿#include "Header.h"

bool load_User_forLogin(char *path, char userLg[][20], char passLg[][20], int &n) {
	ifstream fin(path);
	int i = 0;
	if (!fin.good())	return false;
	while (!fin.eof()) {
		char temp[20];
		char temp2[20];
		fin.get(temp, 20, ',');
		fin.ignore();
		fin.getline(temp2, ',');
		strcpy(userLg[i], temp);
		strcpy(passLg[i], temp2);
		i++;
	}
	n = i;
	fin.close();
	return true;
}

int checkLogin(char *tk, char *mk, int &index, char userAS[][20], char passAS[][20], int nAS, char userL[][20], char passL[][20], int nL, Classroom a[], int nCl) {
	//Check AcademicStaff first
	for (int i = 0; i < nAS; i++) {
		if (strcmp(tk, userAS[i]) == 0 && strcmp(mk, passAS[i]) == 0) {
			index = i;
			return 1;
		}
	}
	//Check lecturer
	for (int i = 0; i < nL; i++) {
		if (strcmp(tk, userL[i]) == 0 && strcmp(mk, passL[i]) == 0) {
			index = i;
			return 2;
		}
	}
	//Check Student
	for (int i = 0; i < nCl; i++) {
		Node <User> *std = a[i].student.head;
		while (std) {
			if (strcmp(std->data.userName, tk) == 0 && strcmp(std->data.pass, mk) == 0) {
				index = i;
				return 0;
			}
			std = std->next;
		}
	}
	return -1;
}

void login(char *tk, char *mk) {
	cout << "UserName: ";
	cin.getline(tk, 100);
	cout << "Password: ";
	string a = passwordInput(100);
	CpyStrnChar(a, mk);
	//cin.getline(mk, 100);
}
void change_pass(Node <User> *student, Classroom a) {
	char tempPass[20];
	char pathChar[50];
	cout << "New Password: ";
	string tmp = passwordInput(20);
	while (tmp == "1") {
		system("cls");
		cout << "Password must be different from 1\n";
		cout << "New Password: ";
		tmp = passwordInput(20);
	}
	CpyStrnChar(tmp, tempPass);
	strcpy(student->data.pass, tempPass);
	string cls = a.className;
	string path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
	CpyStrnChar(path, pathChar);
	save_Class(pathChar, a);
}
void change_pass(char pass[][20], int index) {
	char temp[20];
	cout << "New Password: ";
	string a = passwordInput(20);
	CpyStrnChar(a, temp);
	//cin.getline(temp, 20);
	while (strcmp(temp, "1") == 0) {
		system("cls");
		cout << "Password must be different from 1\n";
		cout << "New Password: ";
		a = passwordInput(20);
		CpyStrnChar(a, temp);
	}
	strcpy(pass[index], temp);
}

// pass more arg in func.
int AcademicStaff(int index, char userAS[][20], char passAS[][20], int nAS) {
	AS_start:
		system("cls");
		cout << "Welcome Academic Staff!\n";
		cout << "0. Show menu\n1. View info\n2. Change password\n3. Logout\n";
		cout << "Which one you want to choose (Press other numbers to exit): ";
		char s[10];
		int c;
		cin.getline(s, 10);
		for (int i = 0; i < strlen(s); i++) {
			if (s[i] < '0' || s[i] > '9')
				return -1;
		}
		c = atoi(s);
		switch (c) {
		case 0:
			menu_AcademicStaff();
			///...///
			goto AS_start;
			break;
		case 1:
			cout << "User: " << userAS[index] << endl;
			cout << "Pass: " << passAS[index] << endl;
			system("pause");
			goto AS_start;
			break;
		case 2:
			system("cls");
			cout << "Change password!\n";
			change_pass(passAS, index);
			saveUser("D:\\Learn\\class\\CS\\PJ\\Project_162\\AcademicStaff.csv", userAS, passAS, nAS);
			goto AS_start;
			break;
		case 3:
			return 0;
		default:
			return -1;
		}
}

// pass more args in function.
void menu_AcademicStaff() {
	menu_Start:
	system("cls");
	cout << "0. Import students of a class from a csv file\n";
	cout << "1. Add a new student to a class\n";
	cout << "2. Edit an existing student\n";
	cout << "3. Remove a student\n";
	cout << "4. Change a student from class A to class B\n";
	cout << "5. Add a new empty class\n";
	cout << "6. View list of class\n";
	cout << "7. View list of students\n";
	cout << "8. Import courses from a csv file\n";
	cout << "9. Add a new course\n";
	cout << "10. Edit an existing course\n";
	cout << "11. Remove a course\n";
	cout << "12. View list of courses\n";
	cout << "13. Import courses'schedules from csv file\n";
	cout << "14. Add a course's schedule\n";
	cout << "15. Edit a course's schedule\n";
	cout << "16. Remove a course's schedule\n";
	cout << "17. View list of schedules\n";
	cout << "18. Search and view attendance list of a course\n";
	cout << "19. Search and view scoreboard of a course\n";
	cout << "Which one you want to choose (Press other numbers to exit): ";
	char chooseChar[10];
	int choose;
	cin >> chooseChar;
	cin.ignore();
	for (int i = 0; i < strlen(chooseChar); i++) {
		if (chooseChar[i] < '0' || chooseChar[i] > '9')
			return;
	}
	choose = atoi(chooseChar);


	string cls, path;
	string clsB, pathB;
	List <Classroom> classes;
	List <Course> courses;
	Node <Course> *tmpCourse;
	Course_Score scot;
	char pathChar[100], pathCharB[100];
	char pathCourse[100] = "D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv";
	Classroom a;
	a.student.head = NULL;
	Classroom b;
	b.student.head = NULL;

	switch (choose) {
	case 0:
		system("cls");
		import_class();
		system("pause");
		goto menu_Start;
		break;
	case 1:
		system("cls");
		cout << "Name of the class you want to add a new student: ";
		getline(cin, cls);
	START0:
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		if (load_class(a, pathChar)) {
			system("cls");
			if (add_aStudent_toClass(a)) {
				save_Class(pathChar, a);
				cout << "Successful addition!\n";
			}
			else {
				cout << "Fail addition!\n";
			}
			remove_allStudents_class(a);
			//system("pause");
		}
		else {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, cls);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_0;
			}
			goto START0;
		}
	END_0:
		system("pause");
		goto menu_Start;
		break;

	case 2:
		system("cls");
		cout << "Name of the class you want to edit a student: ";
		getline(cin, cls);
	START1:
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		a.student.head = NULL;
		if (load_class(a, pathChar)) {
			system("cls");
			if (edit_aStudent(a)) {
				save_Class(pathChar, a);
				cout << "Successful editing!\n";
			}
			else {
				cout << "Editing fail!\n";
			}
			remove_allStudents_class(a);
			goto menu_Start;
		}
		else {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, cls);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_1;
			}
			goto START1;
		}
	END_1:
		system("pause");
		goto menu_Start;
		break;

	case 3:
		system("cls");
		cout << "Name of the class you want to remove a new student: ";
		getline(cin, cls);
	START2:
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		a.student.head = NULL;
		if (load_class(a, pathChar)) {
			system("cls");
			if (remove_aStudent(a)) {
				save_Class(pathChar, a);
				cout << "Successful remove!\n";
			}
			else {
				cout << "Remove fail, exit!\n";
			}
			remove_allStudents_class(a);
		}
		else {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, cls);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_2;
			}
			goto START2;
		}
	END_2:
		system("pause");
		goto menu_Start;
		break;

	case 4:
		system("cls");
		cout << "Name of class A: ";
		getline(cin, cls);
	StartClassA:
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		a.student.head = NULL;
		if (!load_class(a, pathChar)) {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, cls);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_3;
			}
			goto StartClassA;
		}
		cout << "Name of class B: ";
		getline(cin, clsB);
	StartClassB:
		converttouppercase(clsB);
		pathB = "D:\\Learn\\class\\CS\\PJ\\Students\\" + clsB + ".csv";
		CpyStrnChar(pathB, pathCharB);
		b.student.head = NULL;
		if (!load_class(b, pathCharB) || cls == clsB) {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, clsB);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_3;
			}
			goto StartClassB;
		}
		system("cls");
		change_Students(a, b);
		save_Class(pathChar, a);
		save_Class(pathCharB, b);
		END_3:
		remove_allStudents_class(a);
		remove_allStudents_class(b);
		system("pause");
		goto menu_Start;
		break;

	case 5:
		system("cls");
		cout << "Name of class you want to add: ";
		getline(cin, cls);
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		a.student.head = NULL;
		CpyStrnChar(cls, a.className);
		classes = import_listClass();
		if (check_listClass(classes, a)) {
			save_Class(pathChar, a);
			cout << "Successful addition!\n";
		}
		else {
			cout << "Existed class, exit!\n";
		}
		system("pause");
		remove_listClass(classes);
		goto menu_Start;
		break;

	case 6:
		system("cls");
		classes = import_listClass();
		view_listClass(classes);
		remove_listClass(classes);
		system("pause");
		goto menu_Start;
		break;

	case 7:
		system("cls");
		cout << "Name of the class you want to view list of students: ";
		getline(cin, cls);
	START6:
		converttouppercase(cls);
		path = "D:\\Learn\\class\\CS\\PJ\\Students\\" + cls + ".csv";
		CpyStrnChar(path, pathChar);
		a.student.head = NULL;
		if (load_class(a, pathChar)) {
			view_listStudents(a);
			system("pause");
			remove_allStudents_class(a);
		}
		else {
			cout << "Error Class!, Retry or press 0 to exit: ";
			getline(cin, cls);
			if (cls == "0") {
				cout << "Exit!\n";
				goto END_6;
			}
			goto START6;
		}
		END_6:
		goto menu_Start;
		break;
	case 8:
		system("cls");
		import_course();
		system("pause");
		goto menu_Start;
		break;
	case 9:
		system("cls");
		if (load_course(courses, pathCourse)) {
			add_course(courses);
			save_course(pathCourse, courses);
			remove_listCourses(courses);
			cout << "Successful addition!\n";
		}
		else {
			cout << "Loading course Error!\n";
		}
		system("pause");
		goto menu_Start;
		break;

	case 10:
		system("cls");
		if (load_course(courses, pathCourse)) {
			edit_course(courses);
			save_course(pathCourse, courses);
			remove_listCourses(courses);
		}
		else {
			cout << "Loading course Error!\n";
			system("pause");
		}
		goto menu_Start;
		break;

	case 11:
		system("cls");
		if (load_course(courses, pathCourse)) {
			remove_course(courses);
			save_course(pathCourse, courses);
			remove_listCourses(courses);
		}
		else {
			cout << "Loading course Error!\n";
		}
		system("pause");
		goto menu_Start;
		break;

	case 12:
		system("cls");
		if (load_course(courses, pathCourse)) {
			view_course(courses);
			remove_listCourses(courses);
		}
		else {
			cout << "Loading course Error!\n";
		}
		system("pause");
		goto menu_Start;
		break;
	case 13:
		system("cls");
		importSchedule();
		system("pause");
		goto menu_Start;
		break;
	case 14:
		system("cls");
		addschedules();
		system("pause");
		goto menu_Start;
		break;
	case 15:
		system("cls");
		editSchedule();
		system("pause");
		goto menu_Start;
		break;
	case 16:
		system("cls");
		removeSchedule();
		system("pause");
		goto menu_Start;
		break;
	case 17:
		system("cls");
		displaylistofschedules();
		cout << "Choose the class you want to view schedule: ";
		getline(cin, cls);
		tmpCourse = loadSchedule(cls);
		if (!tmpCourse)
			cout << "This class doesn't have shedule!\n";
		else {
			system("cls");
			displaySchedule(tmpCourse);
		}
		removeall(tmpCourse);
		system("pause");
		goto menu_Start;
		break;
	case 18:
		system("cls");
		viewAttendanceList();
		system("pause");
		goto menu_Start;
		break;
	case 19:
		system("cls");
		if (!load_course(courses, pathCourse))
			cout << "Loading course Error!\n";
		else {
			cout << "The course you want to search and view scoreboard: ";
			getline(cin, cls);
			converttouppercase(cls);
			path = "D:\\Learn\\class\\CS\\PJ\\Score\\" + cls + ".csv";
			CpyStrnChar(path, pathChar);
			CpyStrnChar(cls, scot.code);
			if (same_course(scot.code, courses) != NULL) {
				if (load_scoreboard(pathChar, scot)) {
					view_scoreboard_course(scot);
					char choice;
					cout << endl;
					cout << "Do you want to export this scoreboard(y/n): ";
					cin >> choice;
					cin.ignore();
					choice = vertifychoice(choice, true);
					if (choice == 'y') {
						char duongdan[50];
						system("cls");
						cout << "Please input path you want to export scoreboard: ";
						cin.getline(duongdan, 51);
						export_scoreboard(duongdan, scot);
						cout << "Done!\n";
					}
					remove_course_score(scot);
				}
				else {
					cout << "This course doesn't have scoreboard!\n";
				}
			}
			else {
				cout << "This course doesn't exist!\n";
			}
			remove_listCourses(courses);
		}
		system("pause");
		goto menu_Start;
		break;
	default: 
		return;
	}
}

std::string passwordInput(unsigned maxLength) {
	std::string pw;
	for (char c; (c = _getch()); ) {
		if (c == '\n' || c == '\r') { 
			std::cout << "\n";
			break;
		}
		else if (c == '\b') { 
			if (!pw.empty()) {
				std::cout << "\b \b";
				pw.erase(pw.size() - 1);
			}
		}
		else if (c == -32) { 
			_getch(); 
		}
		else if (isprint(c) && pw.size() < maxLength) {
			std::cout << '*';
			pw += c;
		}
	}
	return pw;
}