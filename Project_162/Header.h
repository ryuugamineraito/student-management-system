#ifndef _HEADER_H
#define _HEADER_H_
#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <iostream>
#include <Windows.h>
#include <conio.h>
#include<string>
#include <cctype>
using namespace std;

template <typename T>
struct Node {
	T data;
	Node *prev;
	Node *next;
};

template <typename T>
struct List {
	Node <T> *head;
};

struct User {
	char userName[100];
	char fullname[100];
	char email[200];
	char phone[100];
	int type;
	char pass[100];
	char Class[100];
};
struct date {
	int day, month, year;
};
struct Time {
	int hour, minute;
};

struct Course {
	char code[11];
	char year[21];
	int semester,num_of_date_in_week;
	char name[101];
	char lecturer_user[21];
	date start;
	date end;
	Time from;
	Time to;
	char date_of_week[101];
};

struct Classroom {
	char className[20];
	List <User> student;
};

struct Score {
	char ID[11];
	float midterm, lab, final, bonus;
};

struct Course_Score {
	char code[11];
	char year[21];
	int semester;
	List <Score> score;
};

struct Present {
	char code[11];
	char year[21];
	int semester;
	char studentID[100];
	char *week;
};

//Roles.cpp
void import_class();
bool load_User_forLogin(char *path, char userLg[][20], char passLg[][20], int &n);
int checkLogin(char *tk, char *mk, int &index, char userAS[][20], char passAS[][20], int nAS, char userL[][20], char passL[][20], int nL, Classroom a[], int nCl);
void login(char *tk, char *mk);
void change_pass(char pass[][20], int index);
void change_pass(Node <User> *student, Classroom a);
int AcademicStaff(int index, char userAS[][20], char passAS[][20], int nAS);
void menu_AcademicStaff();
std::string passwordInput(unsigned maxLength);

//Source.cpp
bool load_class(Classroom &a, char path[]);	// load students of a class to data.
void import_aStudent_Screen(User &student);	// input a student .
void import_aStudent_noClass_Screen(User &student);	// input a student without class.
bool add_aStudent_toClass(Classroom &a);	// add a student to a class, use import_aStudent_noClass.
Node <User> *find_aStudent(Classroom a, char id[]);	//find a student with ID.
bool isExisted_ID(Classroom a, char *Id); //check exist.
bool edit_aStudent(Classroom &a);	// not finish	// edit a student in the class (ask the user).
bool remove_aStudent(Classroom &a);	//remove a student from a class (ask the user), we can push "if press 0, program will turn back"
void remove_allStudents_class(Classroom &a); // remove all students in a class
Classroom add_newEClass(char *name); // add a new empty class.
void change_Students(Classroom &a, Classroom &b); //Change students from class A to class B.
void view_listClass(List <Classroom> a); // view list classes.
void view_listStudents(Classroom a); // view list students of a class.
void save_Class(char *path, Classroom a);
void saveUser(char *path, char user[][20], char pass[][20], int n);
//List of Classes.
bool add_tolistClass(Classroom a);
void remove_listClass(List <Classroom> &a);
List <Classroom> import_listClass();
bool check_listClass(List <Classroom> classes, Classroom a);
//Student
int Student(Node <User> *student);
void menu_Student(Node<User> *student);
void load_arrayClass(Classroom *a, int &nCl);
void remove_arrayClass(Classroom *a, int nCl);

//function.cpp
void importSchedule();
Node<Course>* loadSchedule(string classname);
void displaySchedule(Node <Course> *a);
void fixDay(Course &a);
Node<Course>* makenode(Course a);
Node<Course>* viewlistofschedules();
void addschedules();
void displaylistofschedules();
void removeall(Node <Course> *&head);
void CpyStrnChar(string src, char *des);
void inputfromfile(Course &course, ifstream &fin);
void inputfromsrceen(Course& tmp);
bool isCollided(List<Course> a, Course tmp);
char vertifychoice(char choice, bool style);
void inputAgainCmd(Course &tmp);
void outputfile(List <Course> a, ofstream &fout, string classname);
void updateListSchedule(string classname, ofstream &fout);
void converttouppercase(string &classname);
void inputDayStartandEnd(Course &course, ifstream &fin);
bool isExist(char tmp[]);
Node<List <Course>>* wholeSchedules();
bool isCollidedforList(Node<List<Course>> *a, Course tmp);
void inputfromsrceenWithoutCode(Course&tmp);
void editdaybeginandend(List<Course> index);
void editotherthings(List<Course> index);
int compareday(Node<Course>*cur, Course tmp);
void editSchedule();
void updateafterremove(char *tmp);
void removeSchedule();
void converttouppercase(char *name);
void converttolowercase(char *name);
void remove2DList(Node<List <Course>>*&lst);
void viewSchedule(Node <User> *student);
bool isExisted_Class(char *className);
bool isExisted_Code(char *code);

//Course.cpp
void import_course();
void input_with_code(Course &course);
bool load_course(List <Course> &a, char path[]);
void save_course(char path[], List <Course> a);
void add_course(List <Course> &a);
void edit_course(List <Course> &a);
void remove_listCourses(List <Course> &a);
Node <Course> *same_course(char str[], List <Course> a);
void menu_course();
void remove_course(List <Course> &a);
void view_course(List <Course> a);

//ScoreAndLecturer.cpp
int lecturer(int index, char userL[][20], char passL[][20], int nL);
bool load_scoreboard(char path[], Course_Score &b);
void import_scoreboard(char tmpcode[]);
void export_scoreboard(char path[], Course_Score b);
bool edit_grade(Course_Score &b);
void menu_score();
Node <Score> *same_ID(char str[], Course_Score b);
void view_scoreboard_course(Course_Score b);
void remove_course_score(Course_Score &b);
bool check_lecturer(List <Course> courses, char *L_user);
void view_coursesOfLecturer(List <Course> courses, char *L_user);
bool check_courseOfLecturer(List <Course> courses, char *L_user, char *codeCourse);
void menu_Lecturer(char *L_user);
void view_score_student(char id[], List <Course> a);
void list_score_student(char id[], Course_Score b);

//checkin.cpp
int getdayofmonth(int month, int year);
bool leapyear(int year);
int countnumofweek(Node<Course>*tmp);
Node<Present>*makenode(Present tmp);
int getcurrentweek(Node<Course> *tmp);
bool isExist_attendance(char *tmpCoursecode, string classname);
void removeall(Node <Present> *&head);
void removeall(Node <User> *&head);
bool load_attendance(List<Present>&a, Node<Course> *tmpCoursecode, string classname);
void outputfile_attendance(Node<Present>*&present, Node<Course>*&course, ofstream &fout);
void viewcheckInresult(Node<User>*student);
void createAttendanceFile(Node<User> *student, char *tmpCoursecode);
void check_in(Node<User>*student);
int checktime(Node<Course> *tmp);
void export_attendance(List<Present> lst, Node<Course>*cur);
void viewAttendanceList();
int checkday(Node<Course> *cur);











#endif // !_HEADER_H

