#include<iostream>
#include<fstream>
#include<string>
#include <cctype>
#include"Header.h"
#define _CRT_SECURE_NO_WARNINGS
using namespace std;

Node<Course>* makenode(Course a)
{
	Node<Course> *p = new Node<Course>;
	p->data = a;
	p->next = NULL;
	p->prev = NULL;
	return p;
}
void fixDay(Course &a)
{
	if (a.date_of_week[0] == '\n')
		for (int i = 0; i < strlen(a.date_of_week); ++i)
			a.date_of_week[i] = a.date_of_week[i + 1];
}
//read stuff from the file
void inputfromfile(Course &course, ifstream &fin)
{
	char dummy[11];
	fin.ignore(1000, ',');
	fin.get(dummy, 11, ',');
	if (_strcmpi(dummy, " ") != 0)
	{
		strcpy_s(course.code, dummy);
		fin.ignore(1000, ',');
		fin.get(course.name, 101, ',');
		fin.ignore(1000, ',');
		fin.get(course.lecturer_user, 21, ',');
		fin.ignore(1000, ',');
		fin >> course.from.hour;
		fin.ignore(1000, ':');
		fin >> course.from.minute;
		fin.ignore(1000, ',');
		fin >> course.to.hour;
		fin.ignore(1000, ':');
		fin >> course.to.minute;
		//	fin.ignore(1000, ',');
	}
	else
	{
		strcpy_s(course.code, "");
		strcpy_s(course.name, "");
		strcpy_s(course.lecturer_user, "");
		course.from.hour = 0;
		course.from.minute = 0;
		course.to.hour = 0;
		course.to.minute = 0;
		fin.ignore(1000, ',');
	}

}
void importSchedule()
{
	string classname;
	char path[100];
	cout << "PLS INPUT THE PATH YOU WANT TO IMPORT: ";
	cin.get(path, 100, '\n');
	cin.ignore(1000, '\n');
	ifstream fin;
	fin.open(path);
	if (!fin.is_open())
	{
		cout << "Some error happens." << endl;
		return;
	}
	char tmpClass[50];
	fin.get(tmpClass, 50, '\n');//get the information of the class
	classname = tmpClass;
	if (!isExisted_Class(tmpClass)) {
		cout << "This class doesn't exist!\n";
		fin.close();
		return;
	}
	fin.ignore(1000, '\n');
	List<Course> Schedule;
	char dummy[101];
	Course course;
	Node <Course> *cur = NULL;
	Schedule.head = NULL;
	fin >> course.semester;
	fin.ignore(1000, '\n');
	inputDayStartandEnd(course, fin);
	while (!fin.eof())
	{
		fin.get(dummy, 101, ',');
		if (_strcmpi(dummy, "\n") != 0)
		{
			strcpy_s(course.date_of_week, dummy);
		}
		fixDay(course);
		inputfromfile(course, fin);

		if (Schedule.head == NULL)
		{
			Schedule.head = makenode(course);
			cur = Schedule.head;
		}
		else
		{
			cur->next = makenode(course);
			cur->next->prev = cur;
			cur = cur->next;
		}

	}
	fin.close();
	string path2 = "D:\\Learn\\class\\CS\\PJ\\Schedule\\" + classname + ".csv";
	ofstream fout;
	fout.open(path2);
	if (!fout.is_open())
		return;
	outputfile(Schedule, fout, classname);
	if (!isExist(tmpClass))
		updateListSchedule(classname, fout);
	removeall(Schedule.head);
}
//load file from className
Node<Course>* loadSchedule(string classname)
{
	char tmp[50];
	converttouppercase(classname);
	CpyStrnChar(classname, tmp);
	if (isExist(tmp))
	{
		string path = "D:\\Learn\\class\\CS\\PJ\\Schedule\\" + classname + ".csv";
		List<Course> Schedule;
		char dummy[101];
		char tmpClass[50];
		Course course;
		Node <Course> *cur = NULL;
		Schedule.head = NULL;
		ifstream fin;
		fin.open(path);
		if (!fin.is_open())
			return NULL;
		fin.get(tmpClass, 50, '\n');
		fin.ignore(1000, '\n');
		fin >> course.semester;
		fin.ignore(1000, '\n');
		inputDayStartandEnd(course, fin);
		while (!fin.eof())
		{
			fin.get(dummy, 101, ',');
			if (_strcmpi(dummy, "\n") != 0)
			{
				strcpy_s(course.date_of_week, dummy);
			}
			fixDay(course);
			inputfromfile(course, fin);

			if (Schedule.head == NULL)
			{
				Schedule.head = makenode(course);
				cur = Schedule.head;
			}
			else
			{
				cur->next = makenode(course);
				cur->next->prev = cur;
				cur = cur->next;
			}

		}
		fin.close();
		return Schedule.head;
	}
	else return NULL;
}

void displaySchedule(Node <Course> *a)
{
	Node <Course> *cur = a;
	while (cur != NULL)
	{
		if (!(cur != a && strcmp(cur->prev->data.date_of_week, cur->data.date_of_week) == 0))
			cout << cur->data.date_of_week << endl;
		//cout << cur->data.date_of_week << endl;
		cout << cur->data.code << endl;
		cout << cur->data.name << endl;
		cout << cur->data.lecturer_user << endl;
		if (cur->data.from.hour != 0)
		{
			cout << cur->data.from.hour << ":" << cur->data.from.minute << endl;
			cout << cur->data.to.hour << ":" << cur->data.to.minute << endl;
		}
		cout << endl;
		cur = cur->next;

	}
}
//
//return the doubly linked list of list of student in List of student.csv
Node<Course>* viewlistofschedules()
{
	ifstream fin;
	fin.open("D:\\Learn\\class\\CS\\PJ\\Schedule\\Listofschedules.csv");
	if (!fin.is_open())
	{
		cout << "Sorry. Some error happened." << endl;
		return NULL;
	}
	Node<Course> *tmphead = NULL, *cur = NULL;
	Course dummy;

	while (!fin.eof())
	{
		fin.get(dummy.code, 11, '\n');
		if (!tmphead && strcmp(dummy.code, "") == 0) {
			fin.close();
			return tmphead;
		}
		fin.ignore(1000, '\n');
		if (tmphead == NULL)
		{
			tmphead = makenode(dummy);
			cur = tmphead;
		}
		else
		{
			cur->next = makenode(dummy);
			cur->next->prev = cur;
			cur = cur->next;
		}
	}
	fin.close();
	return tmphead;
}
bool isExisted_Class(char *className) {
	bool check = false;
	List <Classroom> a = import_listClass();
	Node <Classroom> *cur = a.head;
	while (cur) {
		if (_strcmpi(className, cur->data.className) == 0) {
			check = true;
			break;
		}
		cur = cur->next;
	}
	remove_listClass(a);
	return check;
}
bool isExisted_Code(char *code) {
	bool check = false;
	List <Course> a;
	load_course(a, "D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv");
	Node<Course> *cur = a.head;
	while (cur) {
		if (_strcmpi(code, cur->data.code) == 0) {
			check = true;
			break;
		}
		cur = cur->next;
	}
	remove_listCourses(a);
	return check;
}
// check code of course.
void addschedules()
{
	string classname;
	cout << "INPUT THE CLASS NAME YOU WANT TO ADD A SCHEDULE: ";
	getline(cin, classname);
	converttouppercase(classname);
	char tmp[50];
	CpyStrnChar(classname, tmp);
	if (!isExisted_Class(tmp)) {
		cout << "This class doesn't exist!\n";
		return;
	}
	if (!isExist(tmp))
	{
		string path = "D:\\Learn\\class\\CS\\PJ\\Schedule\\" + classname + ".csv";
		ofstream fout;
		fout.open(path);
		if (!fout.is_open())
		{
			cout << "Some error happened." << endl;
			return;
		}
		Course tmp;
		List <Course> tmpSchedule;
		Node <Course> *cur;
		tmpSchedule.head = NULL;
		char choice;
		system("cls");
		cout << "Semester: ";
		cin >> tmp.semester;
		cout << "Start: " << endl;
		cout << "Day: ";
		cin >> tmp.start.day;
		cout << "Moth: ";
		cin >> tmp.start.month;
		cout << "Year: ";
		cin >> tmp.start.year;
		cout << "End: " << endl;
		cout << "Day: ";
		cin >> tmp.end.day;
		cout << "Month: ";
		cin >> tmp.end.month;
		cout << "Year: ";
		cin >> tmp.end.year;
		cin.ignore(1000, '\n');
		Node<List<Course>>*lst = wholeSchedules();
		do
		{
			system("cls");
			inputfromsrceen(tmp);
			inputAgainCmd(tmp);
		
			if (!isExisted_Code(tmp.code)) {
				cout << "This course doesn't exist!\n";
				remove2DList(lst);
				fout.close();
				return;
			}
			while (isCollided(tmpSchedule, tmp) || isCollidedforList(lst, tmp))
			{
				system("cls");
				cout << "There will be collided between the course of the schedule." << endl;
				cout << "Please input again." << endl;
				inputfromsrceen(tmp);
			}
			if (tmpSchedule.head == NULL)
			{
				tmpSchedule.head = makenode(tmp);
				cur = tmpSchedule.head;
			}
			else
			{
				cur->next = makenode(tmp);
				cur->next->prev = cur;
				cur = cur->next;
			}
			cout << "Do you want to input the next?(y/n): ";
			cin.get(choice);
			cin.ignore();
			choice = vertifychoice(choice, false);
		} while (tolower(choice) != 'n');
		outputfile(tmpSchedule, fout, classname);
		updateListSchedule(classname, fout);
		displaySchedule(tmpSchedule.head);
		removeall(tmpSchedule.head);
		remove2DList(lst);
		cout << "Successful addition!\n";
	}
	else
		cout << "This class already have." << endl;
}
void removeall(Node <Course> *&head)
{
	Node<Course>*tmp = head;
	while (head != NULL)
	{
		head = head->next;
		delete tmp;
		tmp = head;
	}
}
void displaylistofschedules()
{
	Node<Course>*head = viewlistofschedules();
	Node<Course>*cur = head;
	cout << "The list of schedules: " << endl;
	while (cur != NULL)
	{
		cout << cur->data.code << endl;
		cur = cur->next;
	}
	removeall(head);
}
void CpyStrnChar(string src, char *des)
{
	for (int i = 0; i < src.length(); ++i)
	{
		des[i] = src[i];
		des[i + 1] = '\0';
	}
}
void inputfromsrceen(Course& tmp)
{
	cout << "Day of week(Eg: Monday, Tuesday, ...): ";
	cin.get(tmp.date_of_week, 101, '\n');
	cin.ignore(1000, '\n');
	converttouppercase(tmp.date_of_week);
	cout << "Course code: ";
	cin.get(tmp.code, 11, '\n');
	cin.ignore(1000, '\n');
	converttouppercase(tmp.code);
	cout << "Course name: ";
	cin.get(tmp.name, 101, '\n');
	cin.ignore(1000, '\n');
	inputfromsrceenWithoutCode(tmp);
}
void inputfromsrceenWithoutCode(Course&tmp)
{

	cout << "Lecture username: ";
	cin.get(tmp.lecturer_user, 21, '\n');
	cin.ignore(1000, '\n');
	cout << "From: " << endl;
	cout << "Hour: ";
	cin >> tmp.from.hour;
	cout << "Minute: ";
	cin >> tmp.from.minute;
	cout << "To: " << endl;
	cout << "Hour: ";
	cin >> tmp.to.hour;
	cout << "Minute: ";
	cin >> tmp.to.minute;
	cin.ignore(1000, '\n');
}
//Check collided in the whole list of schedules
bool isCollidedforList(Node<List<Course>> *a, Course tmp)
{
	Node <List<Course>> * cur = a;
	while (cur != NULL)
	{
		while (cur->data.head != NULL)
		{

			if (_strcmpi(cur->data.head->data.date_of_week, tmp.date_of_week) == 0)
			{
				if (cur->data.head->data.to.hour > tmp.from.hour)
					if (cur->data.head->data.end.year == tmp.start.year && cur->data.head->data.end.month>tmp.start.month && _strcmpi(cur->data.head->data.lecturer_user, tmp.lecturer_user) == 0)
						return true;
				if (cur->data.head->data.to.hour == tmp.from.hour && cur->data.head->data.to.minute > tmp.from.minute)
					if (cur->data.head->data.end.year == tmp.start.year && cur->data.head->data.end.month>tmp.start.month && _strcmpi(cur->data.head->data.lecturer_user, tmp.lecturer_user) == 0)
						return true;
			}
			cur->data.head = cur->data.head->next;
		}
		cur = cur->next;
	}
	return false;
}
//
//Check collided the present Schedule
bool isCollided(List<Course> a, Course tmp)
{
	Node <Course> * cur = a.head;
	while (cur != NULL)
	{
		if (_strcmpi(cur->data.date_of_week, tmp.date_of_week) == 0)
		{
			if (cur->data.to.hour > tmp.from.hour)
				return true;
			if (cur->data.to.hour == tmp.from.hour && cur->data.to.minute > tmp.from.minute)
				return true;
		}
		cur = cur->next;
	}
	return false;
}
//
//force user only choose y-yes and n-no
char vertifychoice(char choice, bool style)//true=input again, false=input the next
{
	while (tolower(choice) != 'y'&&tolower(choice) != 'n')
	{
		system("cls");
		cout << "You input wrongly.Please choice again." << endl;
		if (style)
			cout << "Do you want to input again?(y/n): ";
		else
			cout << "Do you want to input the next?(y/n): ";
		cin.get(choice);
		cin.ignore(1000, '\n');

	}
	return choice;
}
//
//check if user want to input again many time
void inputAgainCmd(Course &tmp)
{

	char choice;
	cout << "Do you want to input again?(y/n): ";
	cin.get(choice);
	cin.ignore();
	choice = vertifychoice(choice, true);
	if (choice == 'y')
	{
	START:
		system("cls");
		inputfromsrceen(tmp);
		cout << "Do you want to input again?(y/n): ";
		cin.get(choice);
		cin.ignore();
		choice = vertifychoice(choice, true);
		if (choice == 'y')
			goto START;

	}
}
//
void outputfile(List <Course> a, ofstream &fout, string classname)
{
	Node <Course> *cur = a.head;
	fout << classname << endl;
	fout << cur->data.semester << endl;
	fout << cur->data.start.day << "/" << cur->data.start.month << "/" << cur->data.start.year;
	fout << " - " << cur->data.end.day << "/" << cur->data.end.month << "/" << cur->data.end.year << endl;
	while (cur != NULL)
	{
		if (cur->next != NULL)
		{
			if (cur != a.head&&_strcmpi(cur->prev->data.date_of_week, cur->data.date_of_week) == 0)
				fout << "," << cur->data.code << "," << cur->data.name << "," << cur->data.lecturer_user << "," << cur->data.from.hour << ":" << cur->data.from.minute << "," << cur->data.to.hour << ":" << cur->data.to.minute << endl;
			else
				fout << cur->data.date_of_week << "," << cur->data.code << "," << cur->data.name << "," << cur->data.lecturer_user << "," << cur->data.from.hour << ":" << cur->data.from.minute << "," << cur->data.to.hour << ":" << cur->data.to.minute << endl;
		}
		else
		{
			if (cur != a.head&&_strcmpi(cur->prev->data.date_of_week, cur->data.date_of_week) == 0)
				fout << "," << cur->data.code << "," << cur->data.name << "," << cur->data.lecturer_user << "," << cur->data.from.hour << ":" << cur->data.from.minute << "," << cur->data.to.hour << ":" << cur->data.to.minute;
			else
				fout << cur->data.date_of_week << "," << cur->data.code << "," << cur->data.name << "," << cur->data.lecturer_user << "," << cur->data.from.hour << ":" << cur->data.from.minute << "," << cur->data.to.hour << ":" << cur->data.to.minute;
		}

		cur = cur->next;
	}
	fout.close();
}
void updateListSchedule(string classname, ofstream &fout)
{

	Node <Course> *temp = viewlistofschedules();
	if (!temp)
		fout.open("D:\\Learn\\class\\CS\\PJ\\Schedule\\Listofschedules.csv");
	else
		fout.open("D:\\Learn\\class\\CS\\PJ\\Schedule\\Listofschedules.csv", ios::app);
	fout << classname << endl;
	removeall(temp);
	fout.close();
}
void converttouppercase(string &classname)
{
	for (int i = 0; i < classname.length(); ++i)
	{
		classname[i] = toupper(classname[i]);
	}
}
void converttouppercase(char *name)
{
	for (int i = 0; i < strlen(name); ++i)
	{
		name[i] = toupper(name[i]);
	}
}
void converttolowercase(char *name)
{
	for (int i = 0; i < strlen(name); ++i)
	{
		name[i] = tolower(name[i]);
	}
}
void inputDayStartandEnd(Course &course, ifstream &fin)
{
	fin >> course.start.day;
	fin.ignore(1000, '/');
	fin >> course.start.month;
	fin.ignore(1000, '/');
	fin >> course.start.year;
	fin.ignore(1000, '-');
	fin >> course.end.day;
	fin.ignore(1000, '/');
	fin >> course.end.month;
	fin.ignore(1000, '/');
	fin >> course.end.year;
	fin.ignore(1000, '\n');


}
bool isExist(char tmp[])
{
	int check = false;
	Node<Course> *index = viewlistofschedules();
	Node<Course> *cur = index;
	while (cur != NULL)
	{
		if (_strcmpi(cur->data.code, tmp) == 0)
		{
			check = true;
			break;
		}
		cur = cur->next;
	}
	removeall(cur);
	return check;
}
//Bring all the schedules we have into a list
Node<List <Course>>* wholeSchedules()
{
	Node <Course>* existSchedules = viewlistofschedules();
	Node<List <Course>> *cur = NULL;
	Node<List <Course>>*lst = cur;
	string classname;
	while (existSchedules != NULL)
	{
		classname = existSchedules->data.code;
		if (lst == NULL)
		{
			lst = new Node<List <Course>>;
			lst->data.head = new Node<Course>;
			lst->data.head = loadSchedule(classname);
			lst->next = NULL;
			cur = lst;
		}
		else
		{
			cur->next = new Node<List <Course>>;
			cur = cur->next;
			cur->data.head = new Node<Course>;
			cur->data.head = loadSchedule(classname);
			cur->next = NULL;
		}
		existSchedules = existSchedules->next;
	}
	return lst;
}
void remove2DList(Node<List <Course>>*&lst)
{
	Node<List<Course>>*tmp;
	while (lst != NULL)
	{
		tmp = lst;
		lst = lst->next;
		removeall(tmp->data.head);
		delete tmp;
	}
}

//editSchedule
void editSchedule()
{
	string classname;
	cout << "INPUT THE CLASS NAME YOU WANT TO EDIT ITS SCHEDULE: ";
	getline(cin, classname);
	List <Course> tmp;
	tmp.head = loadSchedule(classname);
	char choice;
	if (tmp.head == NULL) {
		cout << "This class'Schedule doesn't exist." << endl;
		return;
	}

Loop:
	system("cls");
	cout << "Which one do you want to edit:" << endl;
	cout << "1. The beginning day and ending day of the Schedule." << endl;
	cout << "2. The other things." << endl;
	cout << "What is your choice(press other numbers to exit): ";
	char s[10];
	int c;
	cin.getline(s, 10);
	for (int i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9')
			return;
	}
	c = atoi(s);
	switch (c)
	{
	case 1:
		editdaybeginandend(tmp);
		break;
	case 2:
		editotherthings(tmp);
		break;
	default:
		cout << "Exit editing!\n";
		goto END;
	}
	END:
	cout << "Do you want to edit somthing else(y/n)?: ";
	cin.get(choice);
	cin.ignore();
	choice = vertifychoice(choice, true);
	if (choice == 'y')
		goto Loop;


	ofstream fout;
	string path = "D:\\Learn\\class\\CS\\PJ\\Schedule\\" + classname + ".csv";
	fout.open(path);
	outputfile(tmp, fout, classname);
	removeall(tmp.head);
	fout.close();
	cout << "done." << endl;
}
//okay \m/
void editdaybeginandend(List<Course> index)
{
	Course tmp;
Loop:
	system("cls");
	cout << "Start: " << endl;
	cout << "Day: ";
	cin >> tmp.start.day;
	cout << "Moth: ";
	cin >> tmp.start.month;
	cout << "Year: ";
	cin >> tmp.start.year;
	cout << "End: " << endl;
	cout << "Day: ";
	cin >> tmp.end.day;
	cout << "Moth: ";
	cin >> tmp.end.month;
	cout << "Year: ";
	cin >> tmp.end.year;
	cin.ignore(1000, '\n');
	char choice;
	cout << "Do you want to input again?(y/n): ";
	cin.get(choice);
	cin.ignore();
	choice = vertifychoice(choice, true);
	if (choice == 'y')
		goto Loop;
	Node <Course> *cur = index.head;
	while (cur != NULL)
	{
		cur->data.start.day = tmp.start.day;
		cur->data.start.month = tmp.start.month;
		cur->data.start.year = tmp.start.year;
		cur->data.end.day = tmp.end.day;
		cur->data.end.month = tmp.end.month;
		cur->data.end.year = tmp.end.year;
		cur = cur->next;
	}
	Node<List <Course>>*lst = wholeSchedules();
	while (isCollidedforList(lst, index.head->data))
	{
		system("cls");
		cout << "The will be collided between the course of the schedule." << endl;
		cout << "Do you want to input again?(y/n): ";
		cin.get(choice);
		cin.ignore();
		choice = vertifychoice(choice, true);
		if (choice == 'y')
			goto Loop;
		else break;
	}
	remove2DList(lst);
	cout << "Successful." << endl;
}
//okay \m/
void editotherthings(List<Course> index)
{
	Course tmpCourse;
	char choice;
	if (!index.head)	return;
	tmpCourse.start.day = index.head->data.start.day;
	tmpCourse.start.month = index.head->data.start.month;
	tmpCourse.start.year = index.head->data.start.year;
	tmpCourse.end.day = index.head->data.end.day;
	tmpCourse.end.month = index.head->data.end.month;
	tmpCourse.end.year = index.head->data.end.year;
Loop:
	system("cls");
	inputfromsrceen(tmpCourse);
	cout << "Do you want to input again?(y/n): ";
	cin.get(choice);
	cin.ignore();
	choice = vertifychoice(choice, true);
	if (choice == 'y')
		goto Loop;
	if (!isExisted_Code(tmpCourse.code)) {
		cout << "This course doesn't exist!\n";
		return;
	}
	Node<List <Course>>*lst = wholeSchedules();
	while (isCollided(index, tmpCourse) || isCollidedforList(lst, tmpCourse))
	{
		system("cls");
		cout << "The will be collided between the course of the schedule." << endl;
		cout << "Do you want to input again?(y/n): ";
		cin.get(choice);
		cin.ignore();
		choice = vertifychoice(choice, true);
		if (choice == 'y')
			goto Loop;
		else break;
	}
	Node<Course>*cur = index.head;
	while (compareday(cur, tmpCourse) == -1 && cur->next != NULL)
		cur = cur->next;
	while (compareday(cur, tmpCourse) == 0 && cur->data.to.hour <= tmpCourse.from.hour &&cur->next != NULL)
		cur = cur->next;
	if (cur == index.head)
	{
		if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour == tmpCourse.from.hour &&
			cur->data.from.minute == tmpCourse.from.minute && cur->data.to.hour == tmpCourse.to.hour &&
			cur->data.to.minute == tmpCourse.to.minute)
		{
			strcpy_s(cur->data.code, tmpCourse.code);
			strcpy_s(cur->data.name, tmpCourse.name);
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
			strcpy_s(cur->data.code, tmpCourse.code);
		}
		else if (compareday(cur, tmpCourse) == 0 && _strcmpi(cur->data.code, tmpCourse.code) == 0)
		{
			cur->data.from.hour = tmpCourse.from.hour;
			cur->data.from.minute = tmpCourse.from.minute;
			cur->data.to.hour = tmpCourse.to.hour;
			cur->data.to.minute = tmpCourse.to.minute;
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour >= tmpCourse.to.hour && cur->data.from.minute > tmpCourse.to.minute)
		{
			Node<Course> *tmp = makenode(tmpCourse);
			index.head->prev = tmp;
			tmp->next = index.head;
			index.head = tmp;
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.to.hour <= tmpCourse.from.hour&&cur->data.to.minute < tmpCourse.from.minute)
		{
			Node<Course> *tmp = makenode(tmpCourse);
			cur->next->prev = tmp;
			tmp->next = cur->next;
			tmp->prev = cur;
			cur->next = tmp;
		}
		else
		{
			Node<Course> *tmp = makenode(tmpCourse);
			index.head->prev = tmp;
			tmp->next = index.head;
			index.head = tmp;
		}
	}
	else if (cur->next == NULL)
	{

		if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour == tmpCourse.from.hour &&
			cur->data.from.minute == tmpCourse.from.minute && cur->data.to.hour == tmpCourse.to.hour &&
			cur->data.to.minute == tmpCourse.to.minute)
		{
			strcpy_s(cur->data.code, tmpCourse.code);
			strcpy_s(cur->data.name, tmpCourse.name);
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
			strcpy_s(cur->data.code, tmpCourse.code);
		}
		else if (compareday(cur, tmpCourse) == 0 && _strcmpi(cur->data.code, tmpCourse.code) == 0)
		{
			cur->data.from.hour = tmpCourse.from.hour;
			cur->data.from.minute = tmpCourse.from.minute;
			cur->data.to.hour = tmpCourse.to.hour;
			cur->data.to.minute = tmpCourse.to.minute;
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour >= tmpCourse.to.hour && cur->data.from.minute > tmpCourse.to.minute)
		{//add before
			Node<Course> *tmp = makenode(tmpCourse);
			cur->prev->next = tmp;
			tmp->next = cur;
			tmp->prev = cur->prev;
			cur->prev = tmp;
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.to.hour <= tmpCourse.from.hour&&cur->data.to.minute < tmpCourse.from.minute)
		{//add after from end
			Node<Course> *tmp = makenode(tmpCourse);
			cur->next = tmp;
			tmp->prev = cur;
		}
		else if (compareday(cur, tmpCourse) == -1)
		{//if tmpday>tmpcur
			Node<Course> *tmp = makenode(tmpCourse);
			cur->next = tmp;
			tmp->prev = cur;
		}
	}
	else
	{
		if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour == tmpCourse.from.hour &&
			cur->data.from.minute == tmpCourse.from.minute && cur->data.to.hour == tmpCourse.to.hour &&
			cur->data.to.minute == tmpCourse.to.minute)
		{
			strcpy_s(cur->data.code, tmpCourse.code);
			strcpy_s(cur->data.name, tmpCourse.name);
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
			strcpy_s(cur->data.code, tmpCourse.code);
		}
		else if (compareday(cur, tmpCourse) == 0 && _strcmpi(cur->data.code, tmpCourse.code) == 0)
		{
			cur->data.from.hour = tmpCourse.from.hour;
			cur->data.from.minute = tmpCourse.from.minute;
			cur->data.to.hour = tmpCourse.to.hour;
			cur->data.to.minute = tmpCourse.to.minute;
			strcpy_s(cur->data.lecturer_user, tmpCourse.lecturer_user);
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.from.hour >= tmpCourse.to.hour && cur->data.from.minute > tmpCourse.to.minute)
		{//add before
			Node<Course> *tmp = makenode(tmpCourse);
			cur->prev->next = tmp;
			tmp->next = cur;
			tmp->prev = cur->prev;
			cur->prev = tmp;
		}
		else if (compareday(cur, tmpCourse) == 0 && cur->data.to.hour <= tmpCourse.from.hour&&cur->data.to.minute < tmpCourse.from.minute)
		{//add after
			Node<Course> *tmp = makenode(tmpCourse);
			cur->next->prev = tmp;
			tmp->next = cur->next;
			tmp->prev = cur;
			cur->next = tmp;
		}
		else
		{
			Node<Course> *tmp = makenode(tmpCourse);
			cur->prev->next = tmp;
			tmp->next = cur;
			tmp->prev = cur->prev;
			cur->prev = tmp;
		}
	}
	remove2DList(lst);
	cout << "Successful." << endl;
}
int compareday(Node<Course>*cur, Course tmp)
{
	int curday, tmpday;
	if (_strcmpi(cur->data.date_of_week, "MONDAY") == 0)
		curday = 1;
	else if (_strcmpi(cur->data.date_of_week, "TUESDAY") == 0)
		curday = 2;
	else if (_strcmpi(cur->data.date_of_week, "WEDNESDAY") == 0)
		curday = 3;
	else if (_strcmpi(cur->data.date_of_week, "THURSDAY") == 0)
		curday = 4;
	else if (_strcmpi(cur->data.date_of_week, "FRIDAY") == 0)
		curday = 5;
	else if (_strcmpi(cur->data.date_of_week, "SATURDAY") == 0)
		curday = 6;
	if (_strcmpi(tmp.date_of_week, "MONDAY") == 0)
		tmpday = 1;
	else if (_strcmpi(tmp.date_of_week, "TUESDAY") == 0)
		tmpday = 2;
	else if (_strcmpi(tmp.date_of_week, "WEDNESDAY") == 0)
		tmpday = 3;
	else if (_strcmpi(tmp.date_of_week, "THURSDAY") == 0)
		tmpday = 4;
	else if (_strcmpi(tmp.date_of_week, "FRIDAY") == 0)
		tmpday = 5;
	else if (_strcmpi(tmp.date_of_week, "SATURDAY") == 0)
		tmpday = 6;
	if (curday > tmpday)
		return 1;
	else if (curday == tmpday)
		return 0;
	else
		return -1;
}
//removeSchedule
void removeSchedule()
{
	string classname;
	cout << "INPUT THE CLASS NAME YOU WANT TO REMOVE ITS SCHEDULE: ";
	getline(cin, classname);
	char tmp[50], path[200];
	converttouppercase(classname);
	CpyStrnChar(classname, tmp);
	if (isExist(tmp))
	{
		string strpath = "D:\\Learn\\class\\CS\\PJ\\Schedule\\" + classname + ".csv";
		CpyStrnChar(strpath, path);
		remove(path);
		updateafterremove(tmp);
	}
	else
	{
		cout << "This class 'Schedule doesn't exist." << endl;
	}
}
void updateafterremove(char *tmp)
{
	Node<Course> *index = viewlistofschedules();
	Node<Course> *cur = index;
	while (cur != NULL)
	{
		if (_strcmpi(cur->data.code, tmp) == 0)
		{
			if (cur == index)
			{
				Node<Course>*tmp = index;
				index = index->next;
				index->prev = NULL;
				delete tmp;
				break;
			}
			else if (cur->next == NULL)
			{
				Node<Course>*tmp = cur;
				cur->prev->next = cur->next;
				cur = cur->next;
				delete tmp;
				break;
			}
			else
			{
				Node<Course>*tmp = cur;
				cur->prev->next = cur->next;
				cur->next->prev = cur->prev;
				cur = cur->next;
				delete tmp;
				break;
			}

		}
		cur = cur->next;
	}
	ofstream fout;
	fout.open("D:\\Learn\\class\\CS\\PJ\\Schedule\\Listofschedules.csv");
	cur = index;
	while (cur != NULL)
	{
		if (cur->next != NULL)
			fout << cur->data.code << endl;
		else
			fout << cur->data.code;
		cur = cur->next;
	}
	fout.close();
	removeall(index);
}
//34
void viewSchedule(Node <User> *student)	
{
	string classname = student->data.Class;
	Node<Course>*schedule = loadSchedule(classname);
	if (!schedule) {
		cout << "Your class doesn't have schedule!\n";
		return;
	}
	cout << "Class: " << classname << endl;
	cout << "Start: " << schedule->data.start.day << "/" << schedule->data.start.month << "/" << schedule->data.start.year << endl;
	cout << "End: " << schedule->data.end.day << "/" << schedule->data.end.month << "/" << schedule->data.end.year << endl;
	cout << "Semester: " << schedule->data.semester << endl;
	displaySchedule(schedule);
	removeall(schedule);
}

