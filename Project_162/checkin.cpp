#include "Header.h"
#include <ctime>
#include <iomanip>
#include <cstring>
using namespace std;
//tmpCoursecode is the coursecode user want to check attendance
void createAttendanceFile(Node<User> *student,char *tmpCoursecode)
{
	List <Course> lst;
	load_course(lst, "D:\\Learn\\class\\CS\\PJ\\Course\\Course.csv");

	Node<Course> *cur = lst.head;
	while (cur != NULL)//check exist course
	{
		if (_strcmpi(cur->data.code, tmpCoursecode) == 0)
			break;
		cur = cur->next;
	}
	if (cur != NULL)
	{
		int count = 1;
		string filename = cur->data.code;
		string classname = student->data.Class;
		ofstream fout;
		string path = "D:\\Learn\\class\\CS\\PJ\\Attendance\\" + filename + "_" + classname + ".csv";
		char classpath[100];
		string classpathStr = "D:\\Learn\\class\\CS\\PJ\\Students\\" + classname + ".csv";
		CpyStrnChar(classpathStr, classpath);
		Classroom tmp;
		fout.open(path);
		fout << cur->data.code << endl;
		fout << cur->data.year << endl;
		fout << cur->data.semester << endl;
		int numofweek = countnumofweek(cur);
		fout << "No,ID,";
		for (int i = 0; i < numofweek; ++i)
		{
			fout << "Week" << i + 1 << ",";
		}
		fout << endl;
		load_class(tmp, classpath);
		Node<User>*cur_student = tmp.student.head;
		while (cur_student != NULL)
		{
			fout << count << ",";
			fout << cur_student->data.userName << ","<< endl;
			cur_student = cur_student->next;
			++count;
		}
		fout.close();
		removeall(tmp.student.head);
	}
	removeall(lst.head);
	
}
int countnumofweek(Node<Course>*tmp)
{
	int week = 1;
	int day = 0;
	int dayleft;
	if (tmp->data.end.year == tmp->data.start.year)
	{
		if (tmp->data.end.month > tmp->data.start.month)
		{
			for (int i = tmp->data.start.month; i < tmp->data.end.month; ++i)
				day += getdayofmonth(i, tmp->data.end.year);
		}
		day -= (tmp->data.start.day - 1);
		week = day / 7;
		++week;
		dayleft = day % 7;
		for (int i = 1 - dayleft; i <= getdayofmonth(tmp->data.end.month, tmp->data.end.year); i = i + 7)
		{
			if (dayleft != 0)
			{
				if (tmp->data.end.day < i + 7)
					break;
				else
					dayleft = 0;
			}
			else if (tmp->data.end.day < i + 70 && tmp->data.end.day >= i)
				break;
			++week;
		}
	}
	else if (tmp->data.end.year > tmp->data.start.year)
	{
		int i = tmp->data.start.month;
		int j = i;
		while (i !=tmp->data.end.month)
		{
			if (i > 12)
				i = 1;
			if (j <= 12)
				day += getdayofmonth(i, tmp->data.start.year);
			else
				day += getdayofmonth(i, tmp->data.end.year);
			++i;
			++j;
		}
		day -= (tmp->data.start.day - 1);
		week = day / 7;
		++week;
		dayleft = day % 7;
		for (int i = 1 - dayleft; i <= getdayofmonth(tmp->data.end.month, tmp->data.end.year); i = i + 7)
		{
			if (dayleft != 0)
			{
				if (tmp->data.end.day < i + 7)
					break;
				else
					dayleft = 0;
			}
			else if (tmp->data.end.day < i + 7 && tmp->data.end.day >= i)
				break;
			++week;
		}
	}
	return week;
}
bool leapyear(int year)
{
	bool check;
	if (year >= 400)
	{
		if ((year % 4 == 0) || (year % 400 == 0))
			check = true;
		else
			check = false;
	}
	else
	{
		if (year % 4 == 0)
			check = true;
		else
			check = false;
	}
	return check;
}
int getdayofmonth(int month, int year)
{
	bool check = leapyear(year);
	int numberofdays;
	if (check == true)
	{
		if (month == 2)
			numberofdays = 29;
	}
	else
	{
		if (month == 2)
			numberofdays = 28;
	}
	if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12))
		numberofdays = 31;
	else if (month != 2)
		numberofdays = 30;
	return numberofdays;
}
int getcurrentweek(Node<Course> *tmp)
{
	time_t now;
	time(&now);
	tm*pnow = localtime(&now);
	int week = 1;
	int day = 0;
	int dayleft;
	if ((pnow->tm_year + 1990) <= tmp->data.start.year && (pnow->tm_mon + 1) <= tmp->data.start.month && (pnow->tm_mday) < tmp->data.start.day)
		return -1;
	if ((pnow->tm_year + 1990) >= tmp->data.end.year && (pnow->tm_mon + 1) >= tmp->data.end.month && (pnow->tm_mday) > tmp->data.end.day)
		return 0;
	if ((pnow->tm_year + 1990) == tmp->data.start.year)
	{
		if ((pnow->tm_mon + 1) > tmp->data.start.month)
		{
			for (int i = tmp->data.start.month; i < (pnow->tm_mon + 1); ++i)
				day += getdayofmonth(i, (pnow->tm_year + 1990));
		}
		day -= (tmp->data.start.day - 1);
		week = day / 7;
		++week;
		dayleft = day % 7;
		for (int i = 1 - dayleft; i <= getdayofmonth((pnow->tm_mon + 1), (pnow->tm_year + 1990)); i = i + 7)
		{
			if (dayleft != 0)
			{
				if ((pnow->tm_mday) < i + 7)
					break;
				else
					dayleft = 0;
			}
			else if ((pnow->tm_mday) < i + 7 && (pnow->tm_mday) >= i)
				break;
			++week;
		}
	}
	else if ((pnow->tm_year + 1990) > tmp->data.start.year)
	{
		int i = tmp->data.start.month;
		int j = i;
		while (i != (pnow->tm_mon + 1))
		{
			if (i > 12)
				i = 1;
			if (j <= 12)
				day += getdayofmonth(i, tmp->data.start.year);
			else
				day += getdayofmonth(i, (pnow->tm_year + 1990));
			++i;
			++j;
		}
		day -= (tmp->data.start.day - 1);
		week = day / 7;
		++week;
		dayleft = day % 7;
		for (int i = 1 - dayleft; i <= getdayofmonth((pnow->tm_mon + 1), (pnow->tm_year + 1990)); i = i + 7)
		{
			if (dayleft != 0)
			{
				if ((pnow->tm_mday) < i + 7)
					break;
				else
					dayleft = 0;
			}
			else if ((pnow->tm_mday) < i + 7 && (pnow->tm_mday) >= i)
				break;
			++week;
		}
	}
	return week;

}
bool isExist_attendance(char *tmpCoursecode,string classname)
{
	string path = "D:\\Learn\\class\\CS\\PJ\\Attendance\\";
	string code = tmpCoursecode;
	path += code + "_" + classname + ".csv";
	char dummy[10];
	bool check;
	ifstream fin;
	fin.open(path);
	fin.get(dummy, 10,'\n');
	if (strcmp(dummy, "") == 0)
		check = false;
	else check = true;
	fin.close();
	return check;
}
bool load_attendance(List<Present>&a, Node<Course> *tmpCoursecode,string classname)
{
	string path = "D:\\Learn\\class\\CS\\PJ\\Attendance\\";
	string code = tmpCoursecode->data.code;
	path += code + "_" + classname + ".csv";
	ifstream fin;
	fin.open(path);
	if (!fin.is_open())
		return false;
	Node<Present>* cur;
	Present tmp;
	char header[200];
	int numofweek = countnumofweek(tmpCoursecode);
	
	a.head = NULL;
	char dummy[10], dummycount[10];
	
	fin.get(tmp.code, 11, '\n');
	fin.ignore(1000, '\n');
	fin.get(tmp.year, 21, '\n');
	fin.ignore(1000, '\n');
	fin >> tmp.semester;
	fin.ignore(1000, '\n');
	fin.get(header, 200);
	fin.ignore(1000, '\n');
	while (!fin.eof())
	{
		fin.get(dummycount, 10, ',');
		if (strcmp(dummycount, "") == 0)
			break;
		fin.ignore(1000, ',');
		fin.get(tmp.studentID, 100, ',');
		fin.ignore(1000, ',');
		fin.get(dummy, 2, ',');
		if (strcmp(dummy, "\n") != 0)
		{
			int i = 0;
			tmp.week = new char[numofweek];
			memset(tmp.week, ' ', numofweek);
			while (strcmp(dummy, "\n") != 0)
			{
				if (dummy[0] == 'A' || dummy[0] == 'P')
					tmp.week[i] = dummy[0];
				else
					tmp.week[i] = ' ';
				fin.ignore(1000, ',');
				fin.get(dummy, 2, ',');
				++i;
			}
			if (a.head == NULL)
			{
				a.head = makenode(tmp);
				cur = a.head;
			}
			else
			{
				cur->next = makenode(tmp);
				cur->next->prev = cur;
				cur = cur->next;
			}
		}
		else
		{
			tmp.week = new char[numofweek];
			if (a.head == NULL)
			{
				a.head = makenode(tmp);
				cur = a.head;
			}
			else
			{
				cur->next = makenode(tmp);
				cur->next->prev = cur;
				cur = cur->next;
			}
		}

	}
	fin.close();
	return true;
}
Node<Present>*makenode(Present tmp)
{
	Node<Present>*p = new Node<Present>;
	p->data = tmp;
	p->next = NULL;
	p->prev = NULL;
	return p;
}
//need to debug check-in question 31
void check_in(Node<User>*student)
{
	string classname = student->data.Class;
	Node<Course>*ListofCourse = loadSchedule(classname);
	Node<Course>*cur = ListofCourse;
	int count = 0;
	system("cls");
	cout << "You are joining in there course: " << endl;
	while (cur != NULL)
	{
		cout << count++ << "." << cur->data.code << endl;
		cur = cur->next;
	}
	cur = ListofCourse;
	//compareday 
	cout << "Which course do you want to check attendance: ";
	char choice[10];
	cin.get(choice, 10, '\n');
	cin.ignore(1000, '\n');
	int n = atoi(choice);
	if (n > count - 1||n<0)
	{
		cout << "error." << endl;
		return;
	}
	for (int i = 0; i < n; ++i)
		cur = cur->next;
	if (!isExist_attendance(cur->data.code,classname))
		createAttendanceFile(student, cur->data.code);
	if (checkday(cur) != 1)
	{
		cout << "Sorry. You don't have this class to day." << endl;
		removeall(ListofCourse);
		return;
	}
	List<Present> lst;
	lst.head = NULL;
	load_attendance(lst, cur,classname);
	Node<Present>*cur_lst = lst.head;
	int numofweek = countnumofweek(cur);
	int currentweek = getcurrentweek(cur);
	if (currentweek == -1)
	{
		system("cls");
		cout << "Your course doesn't start yet." << endl;
		removeall(ListofCourse);
		removeall(lst.head);
		return;
	}
	else if (currentweek == 0)
		currentweek = numofweek+1;
	while (cur_lst != NULL)
	{
		if (strcmp(cur_lst->data.studentID, student->data.userName) == 0)
			break;
		cur_lst = cur_lst->next;
	}
	for (int i = 0; i < currentweek - 1; ++i)
	{
		if (cur_lst->data.week[i] != 'P' && cur_lst->data.week[i] != 'A')
			cur_lst->data.week[i] = 'A';
	}
	if (currentweek > numofweek)
	{
		cout << "The course is finished. But you can still watch the attendance result." << endl;
		goto END;
	}
	//check-in 
	if (checktime(cur) == -1)
		cout << "Too soon. You can't check attendance." << endl;
	else if (checktime(cur) == 1)
	{
		cout << "You are late.So you absent today." << endl;
		cur_lst->data.week[currentweek - 1] = 'A';

	}
	else
	{
		cout << "Check-in successfully." << endl;
		cur_lst->data.week[currentweek - 1] = 'P';
	}
	END:
	ofstream fout;
	string path = "D:\\Learn\\class\\CS\\PJ\\Attendance\\";
	string coursecode = cur->data.code;
	path += coursecode + "_" + classname + ".csv";
	fout.open(path);
	outputfile_attendance(lst.head, cur, fout);
	removeall(ListofCourse);
}
int checkday(Node<Course> *cur)
{
	int curday;
	time_t now;
	time(&now);
	tm*pnow = localtime(&now);
	if (_strcmpi(cur->data.date_of_week, "MONDAY") == 0)
		curday = 1;
	else if (_strcmpi(cur->data.date_of_week, "TUESDAY") == 0)
		curday = 2;
	else if (_strcmpi(cur->data.date_of_week, "WEDNESDAY") == 0)
		curday = 3;
	else if (_strcmpi(cur->data.date_of_week, "THURSDAY") == 0)
		curday = 4;
	else if (_strcmpi(cur->data.date_of_week, "FRIDAY") == 0)
		curday = 5;
	else if (_strcmpi(cur->data.date_of_week, "SATURDAY") == 0)
		curday = 6;
	else curday = 7;
	if ((pnow->tm_wday) == curday)
		return 1;
	else return 0;
}
void removeall(Node <Present> *&head)
{
	Node<Present>*tmp = head;
	while (head != NULL)
	{
		head = head->next;
		delete tmp;
		tmp = head;
	}
}
void removeall(Node <User> *&head)
{
	Node<User>*tmp = head;
	while (head != NULL)
	{
		head = head->next;
		delete tmp;
		tmp = head;
	}
}
int checktime(Node<Course> *tmp)
{
	time_t now;
	time(&now);
	tm*pnow = localtime(&now);
	if (pnow->tm_hour <= tmp->data.from.hour &&pnow->tm_min < tmp->data.from.minute)
		return -1;//to soon lol
	else if (pnow->tm_hour >= tmp->data.from.hour &&pnow->tm_min > tmp->data.from.minute)
		return 1;//too late lol
	else return 0;//can check attendance
}
void outputfile_attendance(Node<Present>*&present,Node<Course>*&course,ofstream &fout)
{
	int numofweek = countnumofweek(course);
	fout << present->data.code << endl;
	fout << present->data.year << endl;
	fout << present->data.semester << endl;
	fout << "No,ID,";
	for (int i = 0; i < numofweek; ++i)
	{
		fout << "Week" << i + 1 << ",";
	}
	fout << endl;
	Node<Present>*cur = present;
	int count = 1;
	while (cur != NULL)
	{
		fout << count++ << ",";
		fout << cur->data.studentID << ",";
		int i = 0;
		while (cur->data.week[i] == 'P' || cur->data.week[i] == 'A')
		{
			fout << cur->data.week[i] << ",";
			++i;
		}
		fout << endl;
		cur = cur->next;
	}
	fout.close();
	removeall(present);
}
//need to debug question 32
void viewcheckInresult(Node<User>*student)
{
	string classname = student->data.Class;
	Node<Course>*ListofCourse = loadSchedule(classname);
	Node<Course>*cur = ListofCourse;
	int count = 0;
	system("cls");
	cout << "You are joining in there course: " << endl;
	while (cur != NULL)
	{
		cout << count++ << "." << cur->data.code << endl;
		cur = cur->next;
	}
	cur = ListofCourse;
	cout << "Which course do you want to view attendance result: ";
	char choice[10];
	cin.get(choice, 10, '\n');
	cin.ignore(1000, '\n');
	int n = atoi(choice);
	if (n > count - 1 || n<0)
	{
		cout << "error." << endl;
		return;
	}
	for (int i = 0; i < n; ++i)
		cur = cur->next;
	List<Present> lst;
	lst.head = NULL;
	load_attendance(lst, cur, classname);
	Node<Present>*cur_lst = lst.head;
	int numofweek = countnumofweek(cur);
	int currentweek = getcurrentweek(cur);
	if (currentweek == -1)
	{
		system("cls");
		cout << "Your course doesn't start yet." << endl;
		removeall(ListofCourse);
		removeall(lst.head);
		return;
	}
	else if (currentweek == 0)
		currentweek = numofweek;
	while (cur_lst != NULL)
	{
		if (strcmp(cur_lst->data.studentID, student->data.userName) == 0)
			break;
		cur_lst = cur_lst->next;
	}
	system("cls");
	cout << cur_lst->data.code << endl;
	cout << cur_lst->data.year << endl;
	cout << cur_lst->data.semester << endl;
	cout << "ID" << setw(10);
	for (int i = 0; i <numofweek; ++i)
	{
		cout << "Week" << i + 1 << setw(6);
	}
	cout << setw(0);
	cout << endl;
	cout << cur_lst->data.studentID << setw(3);
	int i = 0;
	while (cur_lst->data.week[i] == 'P' || cur_lst->data.week[i] == 'A' )
	{
		if (i >= 9)
			cout << cur_lst->data.week[i] << setw(8);
		else
			cout << cur_lst->data.week[i] << setw(7);
		++i;
	}
	cout << endl;
	removeall(lst.head);
	removeall(ListofCourse);
}
//need to debug question 24
void viewAttendanceList()
{
	//string path = "D:\\Learn\\class\\CS\\PJ\\Attendance\\";
	char tmp[50];
	char tmp1[50];
	system("cls");
	cout << "PLEASE INPUT THE COURSE YOU WANT TO SEE ATTENDANCE LIST: ";
	cin.get(tmp, 50, '\n');
	cin.ignore(1000, '\n');
	//string course = tmp;
	cout << "PLEASE INPUT THE CLASS YOU WANT TO SEE ATTENDANCE LIST: ";
	cin.get(tmp1, 50, '\n');
	cin.ignore(1000, '\n');
	string classname = tmp1;
	//path += course + "_" + classname + ".csv";
	if (!isExist_attendance(tmp,classname))
	{
		cout << "This course doesn't exist." << endl;
		return;
	}
	List<Present> lst;//remember to remove
	lst.head = NULL;
	Node<Course>* tmpCourse = loadSchedule(classname);//remember to remove
	Node<Course>*cur = tmpCourse;
	while (cur != NULL)
	{
		if (_strcmpi(cur->data.code, tmp) == 0)
			break;
		cur = cur->next;
	}
	load_attendance(lst, cur,classname);
	Node<Present> *cur_lst = lst.head;
	cout << cur_lst->data.code << endl;
	cout << cur_lst->data.year << endl;
	cout << cur_lst->data.semester << endl;
	cout << "No" << setw(5) << "ID" << setw(10);
	int numofweek = countnumofweek(cur);
	for (int i = 0; i <numofweek ; ++i)
	{
		if (i == numofweek - 1)
			cout << "Week" << i + 1;
		else
			cout << "Week" << i + 1 << setw(6);
	}
	cout << endl;
	int count = 1;
	int i = 0;
	while (cur_lst != NULL)
	{
		cout << setw(0);
		if (count >= 10)
			cout << count++ << setw(3) << " " << cur_lst->data.studentID << setw(3);
		else
			cout << count++ << setw(4) << " " << cur_lst->data.studentID << setw(3);
		int i = 0;
		while (cur_lst->data.week[i] == 'P' || cur_lst->data.week[i] == 'A')
		{
			if (i >= 9)
				cout << cur_lst->data.week[i] << setw(8);
			else
				cout << cur_lst->data.week[i] << setw(7);
			++i;
		}
		cout << endl;
		cur_lst = cur_lst->next;
	}
	char choice;
	cout << endl;
	cout << "Do you want to export this attendance: ";
	cin >> choice;
	cin.ignore();
	choice = vertifychoice(choice, true);
	if (choice == 'y')
		export_attendance(lst, cur);
	else
		removeall(lst.head);
	removeall(tmpCourse);
}
//need to debug question 25
void export_attendance(List<Present> lst,Node<Course>*cur)
{
	char path[100];
	system("cls");
	cout << "PLEASE INPUT THE PATH YOU WANT TO EXPORT ATTENDANCE LIST: ";
	cin.get(path, 100, '\n');
	cin.ignore(1000, '\n');

	Node<Present> *cur_lst = lst.head;
	ofstream fout;
	fout.open(path);
	outputfile_attendance(lst.head, cur, fout);//this function removed lst.head
	cout << "Successful." << endl;

}

